package com.zen.tlmn;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.graphics.Pixmap;

import java.text.NumberFormat;

import ze.GMain;


public class AndroidLauncher extends AndroidApplication {
	private ZenSDK zenSDK;
	public static GMain game;
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		zenSDK = new ZenSDK(this);

		game = new GMain(zenSDK);

		View libgdxview = initializeForView(game, config);
		zenSDK.OnCreate(libgdxview);
	}

	@Override
	protected void onPause() {
		zenSDK.onPause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		zenSDK.onResume();
	}

	@Override
	protected void onDestroy() {
		zenSDK.onDestroy();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		zenSDK.onActivityResult(requestCode, resultCode, data);
	}
}
