package com.zen.tlmn;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by BaDuy on 4/23/2018.
 */

public class TextureDownload {
    private Texture texture;
    public TextureRegionDrawable drawable;
    public TextureDownload (final String url,int width, int height) {
        drawable = new TextureRegionDrawable(new TextureRegion(texture,0,0,width,height));

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            @Override
            public void run () {
         try {
             byte[] bytes = HttpRequest.SendGetRequestToByte(url);
             if (bytes != null) {
                 // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                 Pixmap pixmap = new Pixmap(bytes, 0, bytes.length);
                 final int originalWidth = pixmap.getWidth();
                 final int originalHeight = pixmap.getHeight();
                 int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                 int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                 final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                 potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                 pixmap.dispose();
                 Gdx.app.postRunnable(new Runnable() {
                     @Override
                     public void run() {
                         Texture txt = new Texture(potPixmap);
                         texture.load(txt.getTextureData());
                         drawable.getRegion().setRegion(0, 0, originalWidth, originalHeight);
                     }
                 });
             }
         }
         catch (Exception e){

         }
            }
        }).start();
    }
}
