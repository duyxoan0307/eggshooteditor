package com.zen.tlmn;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.ScreenUtils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.LeaderboardsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import ze.platform.IPlat;

/**
 * Created by BaDuy on 5/25/2018.
 */

public class ZenSDK implements IPlat {

    private static final String TAG = "AndroidLauncher";
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;
    FrameLayout rootView;
    AdView adView;
    InterstitialAd interstitialAd;
    RewardedVideoAd rewardedVideoAd;
    long lastInterstitialTime;
    RewardItem rewardItemReturn;
    boolean isVideoLoad = false;
    private GoogleSignInClient mGoogleSignInClient;
    private LeaderboardsClient mLeaderboardsClient;

    private static final int RC_LEADERBOARD_UI = 9004;
    private static final int RC_UNUSED = 5001;
    private static final int RC_SIGN_IN = 9001;

    private static final String LEADERBOARD_ID = "CgkIwZyU5fANEAIQAA";
    private static final String ANALYTICS_SCREENNAME= "TLDEMLA";

    private static final String ADMOB_APP_ID = "ca-app-pub-1015023790649631~4335688220";
    private static final String ADMOB_BANNER_ID = "";
    private static final String ADMOB_FULLSCREEN_ID = "ca-app-pub-1015023790649631/6842555779";
    private static final String ADMOB_VIDEO_ID = "ca-app-pub-1015023790649631/9005560970";

    private IPlat.OnVideoRewardClosed videoRewardCallback = null;

    boolean bannerVisible = true;

    private AndroidLauncher androidLauncher;

    public ZenSDK(AndroidLauncher android) {
        this.androidLauncher = android;
    }

    public void OnCreate(View libgdxview) {

        rootView = new FrameLayout(androidLauncher);
        rootView.addView(libgdxview);
        androidLauncher.setContentView(rootView);

        InitAd();
        InitGA();

        InitLeaderboard();
    }

    public void ShowFullscreen() {
        Gdx.app.log("ad_test","Show full screen");
        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (System.currentTimeMillis() - lastInterstitialTime > 180000) {

                    if (interstitialAd.isLoaded()) {
                        interstitialAd.show();
                        lastInterstitialTime = System.currentTimeMillis();
                        Gdx.app.log("ad_test","Show qc");
                    }
                    else

                        Gdx.app.log("ad_test","chua load ad");
                }
                else

                    Gdx.app.log("ad_test","chua den gio`");
            }
        });

    }

    @Override
    public void ShowBanner(boolean visible) {
        ShowGameBanner(visible);
        bannerVisible = visible;
    }

    @Override
    public void TrackLevelStart(int level) {
        Tracker tracker = getDefaultTracker();
        tracker.send(new HitBuilders.EventBuilder("LevelEnter", "" + level).build());
    }

    @Override
    public void TrackLevelFailed(int level) {
        Tracker tracker = getDefaultTracker();
        tracker.send(new HitBuilders.EventBuilder("LevelFailed", "" + level).build());
    }

    @Override
    public void TrackLevelCompleted(int level) {
        Tracker tracker = getDefaultTracker();
        tracker.send(new HitBuilders.EventBuilder("LevelCompleted", "" + level).build());
    }

    @Override
    public void ShowLeaderBoard() {
        try {
            ShowGameLeaderBoard();
        }
        catch(Exception e){

        }
    }

    @Override
    public boolean IsVideoRewardReady() {
        return isVideoLoad;
    }

    @Override
    public void ShowVideoReward(IPlat.OnVideoRewardClosed callback) {
        videoRewardCallback = callback;
        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (rewardedVideoAd.isLoaded()) {
                    rewardedVideoAd.show();
                }
            }
        });
    }

    @Override
    public void FBInstant_GetDoubleStats(String statname, FBInstant_GetStatsCallback callback) {

    }

    @Override
    public void FBInstant_SetDoubleStats(String statname, double value, FBInstant_SetStatsCallback callback) {

    }

    @Override
    public void FBInstant_IncrementDoubleStats(String statname, double value, FBInstant_IncrementDoubleStatsCallback callback) {

    }

    @Override
    public void FBInstant_LoadLeaderboard(FBInstant_LeaderboardEntryCallback callback) {

    }

    @Override
    public void FBInstant_LoadLeaderboardFriend(FBInstant_LeaderboardEntryCallback callback) {

    }

    @Override
    public void FBInstant_GetPlayerInfo(FBInstant_PlayerInfoCallback callback) {

    }

    @Override
    public String FBInstant_GetPlayerName() {
        return null;
    }

    @Override
    public String FBInstant_GetPlayerPhoto() {
        return null;
    }

    @Override
    public String FBInstant_GetPlayerID() {
        return null;
    }

    @Override
    public boolean FBInstant_IsPreloadLoadingFinished(int num) {
        return false;
    }

    @Override
    public void FBInstant_StartLoadingPreload(int num) {

    }

    @Override
    public void FBInstant_ReportScore(double score) {

    }

    @Override
    public void OnShow() {

    }

    @Override
    public void FBInstant_CanCreateShortCut(FBInstant_CanCreateCallback callback) {

    }

    @Override
    public void FBInstant_CreateShortCut() {

    }

    @Override
    public void FBInstant_CanSubscribeBot() {

    }

    @Override
    public String FBInstant_GetDataContext() {
        return null;
    }

    @Override
    public void FBInstant_ShareAsync(Pixmap pixmap, FBInstant_ContextCallback callback) {

    }

    @Override
    public void FBInstant_InviteAsync(FBInstant_ContextCallback callback) {

    }

    @Override
    public void FBInstant_UpdateAsync(String data, FBInstant_ContextCallback callback) {

    }

    @Override
    public String FBInstant_Getlocale() {
        return null;
    }

    @Override
    public void ReportScore(String leaderboardID, long score) {
        ReportGameScore(leaderboardID, score);
    }


    @Override
    public void log(String str) {

    }

    @Override
    public void LoadUrlTexture(String url, UrlTextureCallback callback) {

    }

    @Override
    public Pixmap getFrameBufferPixmap(int x, int y, int w, int h) {
        byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), true);

        for(int i = 4; i < pixels.length; i += 4) {
            pixels[i - 1] = (byte) 255;
        }

        Pixmap pixmap = new Pixmap(Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), Pixmap.Format.RGBA8888);
        BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
        return pixmap;
    }

    @Override
    public String getBase64URL(Pixmap pixmap) {
        return null;
    }

    @Override
    public String GetNumberFormat(long number) {
        return number+"";
    }

    @Override
    public String GetNumberDotFormat(long number) {
        return null;
    }

    @Override
    public void FBInstant_GetJsonStats(FBInstant_GetJsonStatsCallback callback) {

    }

    @Override
    public void FBInstant_SetJsonStats(String value, FBInstant_SetJsonStatsCallback callback) {

    }

    @Override
    public void FBInstant_GetStringData(String statname, FBInstant_GetStringStatsCallback callback) {

    }

    @Override
    public void FBInstant_SetStringData(String statname, String value, FBInstant_SetStringStatsCallback callback) {

    }


    GoogleSignInAccount mSignedInAccount = null;

    private void onConnected(GoogleSignInAccount googleSignInAccount) {
        Log.d(TAG, "onConnected(): connected to Google APIs");
        if (mSignedInAccount != googleSignInAccount) {

            mSignedInAccount = googleSignInAccount;
        }

    }

    public void ShowGameBanner(boolean visible) {
        final boolean v = visible;
        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (v)
                    adView.setVisibility(View.VISIBLE);
                else
                    adView.setVisibility(View.GONE);
            }
        });

    }

    public void RateApp() {
        try {
            String packageName = androidLauncher.getApplicationContext().getPackageName();
            androidLauncher.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        }
        catch (Exception e){

        }
    }

    public void ReportGameScore(String leaderboardID, long score) {
        try {
            if (isSignedIn()) {
                Games.getLeaderboardsClient(androidLauncher, GoogleSignIn.getLastSignedInAccount(androidLauncher))
                        .submitScore(LEADERBOARD_ID, score);
            }
        }
        catch (Exception e){

        }
    }

    public void ShowGameLeaderBoard() {
        try {
            if (isSignedIn()) {
                Games.getLeaderboardsClient(androidLauncher, GoogleSignIn.getLastSignedInAccount(androidLauncher))
                        .getLeaderboardIntent(LEADERBOARD_ID)
                        .addOnSuccessListener(new OnSuccessListener<Intent>() {
                            @Override
                            public void onSuccess(Intent intent) {
                                try {
                                    androidLauncher.startActivityForResult(intent, RC_LEADERBOARD_UI);
                                }
                                catch(Exception e){}
                            }
                        });
            } else {
                forceSignIn = true;
                signInSilently(new Runnable() {
                    @Override
                    public void run() {
                        ShowGameLeaderBoard();
                    }
                });
            }
        }
        catch (Exception e){

        }
    }

    public void InitAd() {
        MobileAds.initialize(androidLauncher, ADMOB_APP_ID);

        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    initVideoReward();
                    initBanner();
                    initInterstitial();
                }
                catch (Exception e){

                }
            }
        });

    }

    void initVideoReward() {
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(androidLauncher);
        rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                Gdx.app.log("VIDEO", "onRewardedVideoAdLoaded");
                isVideoLoad = true;
            }

            @Override
            public void onRewardedVideoAdOpened() {
                Gdx.app.log("VIDEO", "onRewardedVideoAdOpened");
            }

            @Override
            public void onRewardedVideoStarted() {
                Gdx.app.log("VIDEO", "onRewardedVideoStarted");
            }

            @Override
            public void onRewardedVideoAdClosed() {
                Gdx.app.log("VIDEO", "onRewardedVideoAdClosed");

                if (videoRewardCallback != null) {
                    if (rewardItemReturn != null)
                        videoRewardCallback.OnEvent(true);
                    else
                        videoRewardCallback.OnEvent(false);
                    videoRewardCallback = null;
                }

                isVideoLoad = false;
                loadRewardedVideoAd();


            }

            @Override
            public void onRewarded(RewardItem rewardItem) {
                lastInterstitialTime = System.currentTimeMillis();
                rewardItemReturn = rewardItem;

                Gdx.app.log("VIDEO", "onRewarded");

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {
                Gdx.app.log("VIDEO", "onRewardedVideoAdLeftApplication");
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {
                Gdx.app.log("VIDEO", "onRewardedVideoAdFailedToLoad");

                isVideoLoad = false;
                count_load_video_reward++;
                if (count_load_video_reward<5)
                    loadRewardedVideoAd();
            }

            @Override
            public void onRewardedVideoCompleted() {

            }
        });
        loadRewardedVideoAd();
    }

    static int count_load_video_reward=0;

    private void loadRewardedVideoAd() {

        try {
            rewardItemReturn = null;
            AdRequest.Builder adrequest = new AdRequest.Builder();
            adrequest.addTestDevice("89B71FB49AC29A5C3BB3416D43112D55");

            rewardedVideoAd.loadAd(ADMOB_VIDEO_ID, adrequest.build());
        }
        catch(Exception e){}
    }


    void initInterstitial() {
        interstitialAd = new InterstitialAd(androidLauncher);
        interstitialAd.setAdUnitId(ADMOB_FULLSCREEN_ID);
        interstitialAd.loadAd(new AdRequest.Builder().build());
        lastInterstitialTime = System.currentTimeMillis();

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLeftApplication() {
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdClosed() {
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    void initBanner() {
        adView = new AdView(androidLauncher);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(ADMOB_BANNER_ID);//
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        adView.setLayoutParams(params);


        final LinearLayout layout = new LinearLayout(androidLauncher);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setGravity(Gravity.BOTTOM);
        layout.addView(adView);


        rootView.addView(layout);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.GONE);
                //adView.setVisibility(View.VISIBLE);
                //layout.addView(adView);
                //Log.d("TEST", "AdLoaded");

                ShowGameBanner(bannerVisible);

            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                adView.setVisibility(View.INVISIBLE);

            }
        });


        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);


    }

    void InitGA() {
        sAnalytics = GoogleAnalytics.getInstance(androidLauncher);
        Tracker tracker = getDefaultTracker();
        tracker.setScreenName(ANALYTICS_SCREENNAME);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());


    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }


    private boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(androidLauncher) != null;
    }

    boolean forceSignIn = false;

    Runnable signinAction;

    private void signInSilently(Runnable action) {
        signinAction = action;
        mGoogleSignInClient.silentSignIn().addOnCompleteListener(androidLauncher,
                new OnCompleteListener<GoogleSignInAccount>() {
                    @Override
                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInSilently(): success");
                            if (signinAction != null)
                                signinAction.run();

                            signinAction = null;
                            //onConnected(task.getResult());
                        } else {
                            Log.d(TAG, "signInSilently(): failure", task.getException());
                            //onDisconnected();
                            if (forceSignIn) {
                                startSignInIntent();
                            }
                        }
                    }
                });
    }

    private void startSignInIntent() {
        try {
            androidLauncher.startActivityForResult(mGoogleSignInClient.getSignInIntent(), RC_SIGN_IN);
        }
        catch(Exception e){}
    }

    private void signOut() {
        Log.d(TAG, "signOut()");

        if (!isSignedIn()) {
            Log.w(TAG, "signOut() called, but was not signed in!");
            return;
        }

        mGoogleSignInClient.signOut().addOnCompleteListener(androidLauncher,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        boolean successful = task.isSuccessful();
                        Log.d(TAG, "signOut(): " + (successful ? "success" : "failed"));


                    }
                });
    }

    void InitLeaderboard() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(androidLauncher, gso);
    }

    //android Event
    public void onResume() {
        rewardedVideoAd.resume(androidLauncher);
        Log.d(TAG, "onResume()");

        // Since the state of the signed in user can change when the activity is not active
        // it is recommended to try and sign in silently from when the app resumes.
        //signInSilently();
    }
    public void onPause() {
        rewardedVideoAd.pause(androidLauncher);
    }
    public void onDestroy() {
        rewardedVideoAd.destroy(androidLauncher);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task =
                    GoogleSignIn.getSignedInAccountFromIntent(intent);

            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                onConnected(account);
                Log.d("SignIn", "SignIn");

                if (signinAction != null) {
                    signinAction.run();
                }
                signinAction = null;
            } catch (ApiException apiException) {
                String message = apiException.getMessage();
                if (message == null || message.isEmpty()) {
                    message = "Sign In Error";
                }

                //onDisconnected();

                new AlertDialog.Builder(androidLauncher)
                        .setMessage(message)
                        .setNeutralButton(android.R.string.ok, null)
                        .show();
            }
        }
    }



}
