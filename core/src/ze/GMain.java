package ze;

import com.badlogic.gdx.graphics.g2d.Batch;

import ze.gamelogic.other.FBData;
import ze.platform.IPlat;

import ze.core.util.GDirectedGame;
import ze.core.util.GStage;
import ze.gamelogic.screen.MenuScreen;

public class GMain extends GDirectedGame
{
  public static GMain me;
  public static int screenHeight = 0;
  public static int screenId = -1;
  public static String version = "tl100";

  public static IPlat getPlatform() {
    return platform;
  }

  static IPlat platform;
  public GMain(IPlat plat){
    platform = plat;
    me = this;
  }

  private void init()
  {
    float height = 1280f;
    float width=1280;


    //width = Gdx.graphics.getWidth();
    //height = Gdx.graphics.getHeight();

    GStage.init(width, height, -width/2, -height/2, new GStage.StageBorder() {
      @Override
      public void drawHorizontalBorder(Batch spriteBatch, float paramFloat1, float paramFloat2) {
//        float f3 = - paramFloat2;
//        GShapeTools.drawRectangle((SpriteBatch)spriteBatch, Color.RED, (float)0.0f, (float)paramFloat2, (float)paramFloat1, (float)f3, (boolean)true);
//        GShapeTools.drawRectangle((SpriteBatch)spriteBatch, Color.RED, (float)0.0f, (float)848.0f, (float)paramFloat1, (float)f3, (boolean)true);

      }

      @Override
      public void drawVerticalBorder(Batch spriteBatch, float paramFloat1, float paramFloat2) {
//        float f3 = - paramFloat2;
//        GShapeTools.drawRectangle((SpriteBatch)spriteBatch, Color.RED, (float)paramFloat2, (float)0.0f, (float)f3, (float)paramFloat1, (boolean)true);
//        GShapeTools.drawRectangle((SpriteBatch)spriteBatch, Color.RED, (float)480.0f, (float)0.0f, (float)f3, (float)paramFloat1, (boolean)true);

      }
    });
  }
  //xxxxxx
  public static MenuScreen menuScreen(){
    return new MenuScreen();
  }

  public void create()
  {
    getPlatform().OnShow();
    this.init();
    this.setScreen(menuScreen());
  }

  public void dispose()
  {
    super.dispose();
  }

  public void startGame(){
    FBData.syncData();
    FBData.askShortcut();
  }
}