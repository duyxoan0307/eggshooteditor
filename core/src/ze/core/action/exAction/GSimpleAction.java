package ze.core.action.exAction;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GSimpleAction extends Action {
   private GSimpleAction.ActInterface actInterface;

   public static GSimpleAction simpleAction(GSimpleAction.ActInterface actInterface) {
      GSimpleAction var1 = (GSimpleAction)Actions.action(GSimpleAction.class);
      var1.actInterface = actInterface;
      return var1;
   }

   public boolean act(float dt) {
      return this.actInterface.act(dt, this.actor);
   }

   public interface ActInterface {
      boolean act(float dt, Actor actor);
   }
}
