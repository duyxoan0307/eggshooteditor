package ze.core.script;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import ze.core.util.GTools;

import java.util.Vector;

public abstract class GEvent {
   public static final int EVENT_TYPE_SIMPLE = 0;
   public static final int EVENT_TYPE_TREE = 1;
   static final String E_SET_BOOL = "setBool";
   static final String E_SET_INT = "setInt";
   static final String E_SET_STRING = "setString";
   static final String L_BREAK = "break";
   static final String L_CALL = "call";
   static final String L_ELSE = "else";
   static final String L_ENDIF = "endIf";
   static final String L_ENDLOOP = "endLoop";
   static final String L_ENDSYNC = "endSync";
   static final String L_IF = "if";
   static final String L_IF_EX = "if_ex";
   static final String L_LEFT = "{";
   static final String L_LOOP = "loop";
   static final String L_LOOP_EX = "loop_ex";
   static final String L_RIGHT = "}";
   static final String L_SET_RESULT = "setResult";
   static final String L_SYNC = "sync";
   public static final int STATUS_END = 2;
   public static final int STATUS_GO = 1;
   public static final int STATUS_READY = 0;
   protected GEvent.Data data;
   private boolean isAutoReset = false;
   protected String result;
   protected GEvent.SNode root = new GEvent.SNode((GEvent.SNode)null);
   protected GScript script;

   protected static boolean getBool(String var0) {
      return var0.equals("true")?true:(var0.equals("false")?false:Boolean.parseBoolean(var0));
   }

   protected static boolean getBoolEX(String var0, String var1) {
      return true;
   }

   protected static float getFloat(String var0) {
      return Float.parseFloat(var0);
   }

   protected static int getInt(String var0) {
      return Integer.parseInt(var0);
   }

   protected static String getString(String var0) {
      return var0;
   }

   protected static GEvent newInstance(GScript var0, GEvent.Data var1) {
      int var2 = var0.getEventType();
      Object var3 = null;
      switch(var2) {
      case 0:
         var3 = new GEvent_Simple();
         break;
      case 1:
         var3 = new GEvent_Tree();
      }

      ((GEvent)var3).init(var0, var1);
      return (GEvent)var3;
   }

   public static String[] spliteOrder(String var0) {
      int var1 = 0;
      String[] var4 = GTools.splitString(var0.substring(1, var0.length()).trim(), " ");
      if(var4.length <= 1) {
         return var4;
      } else {
         String[] var2 = GTools.splitString(var4[1], ",");
         String[] var3 = new String[var2.length + 1];

         for(var3[0] = var4[0]; var1 < var2.length; ++var1) {
            var3[var1 + 1] = var2[var1];
         }

         return var3;
      }
   }

   public String getID() {
      return this.data.getHeader("eventID");
   }

   public String getResult() {
      return this.result;
   }

   public abstract int getStatus();

   protected abstract void init(GScript var1, GEvent.Data var2);

   protected boolean isAutoReset() {
      return this.isAutoReset;
   }

   public abstract void reset();

   protected abstract void run(Object var1);

   public void setAutoReset(boolean var1) {
      this.isAutoReset = var1;
   }

   protected abstract void setEnd(GEvent.SOrder var1);

   protected abstract void setStatus(int var1);

   abstract static class BaseOrder {
      GEvent.SNode parent;
      int status;

      protected BaseOrder(GEvent.SNode var1, int var2) {
         this.parent = var1;
         this.status = var2;
      }

      public abstract void init();
   }

   static class Data {
      private ObjectMap header = new ObjectMap();
      private Array order = new Array();

      public Data(JsonValue var1) {
         for(int var2 = 0; var2 < var1.size; ++var2) {
            String var4 = var1.get(var2).name;
            if(var4.equals("script")) {
               JsonValue var5 = var1.get(var2);

               for(int var3 = 0; var3 < var5.size; ++var3) {
                  this.order.add(GEvent.spliteOrder(var5.getString(var3)));
               }
            } else {
               this.header.put(var4, var1.getString(var2));
            }
         }

      }

      public String getHeader(String var1) {
         return (String)this.header.get(var1);
      }

      public String[] getOrder(int var1) {
         return (String[])this.order.get(var1);
      }

      public int getOrderSize() {
         return this.order.size;
      }

      public String[][] getOrders() {
         String[][] var2 = new String[this.getOrderSize()][];

         for(int var1 = 0; var1 < var2.length; ++var1) {
            var2[var1] = this.getOrder(var1);
         }

         return var2;
      }
   }

   static class SNode extends GEvent.BaseOrder {
      int curLine;
      private Vector data = new Vector();
      int type;

      protected SNode(GEvent.SNode var1) {
         super(var1, 0);
      }

      public void addOrder(int var1, String[] var2) {
         this.addOrder(new GEvent.SOrder(this, var1, var2));
      }

      public void addOrder(GEvent.BaseOrder var1) {
         this.data.add(var1);
      }

      public GEvent.BaseOrder getOrder(int var1) {
         return (GEvent.BaseOrder)this.data.get(var1);
      }

      public int getSize() {
         return this.data.size();
      }

      public void init() {
         int var1 = 0;
         this.status = 0;

         for(this.curLine = 0; var1 < this.data.size(); ++var1) {
            this.getOrder(var1).init();
         }

      }

      public boolean isEmpty() {
         return this.data.isEmpty();
      }

      public boolean isNode(int var1) {
         return ((GEvent.BaseOrder)this.data.get(var1)).getClass() == GEvent.SNode.class;
      }

      public int lastLine() {
         return this.data.size() - 1;
      }
   }

   public static class SOrder extends GEvent.BaseOrder {
      String[] order;

      protected SOrder(GEvent.SNode var1, int var2, String[] var3) {
         super(var1, var2);
         this.order = var3;
      }

      public boolean getBool(int var1) {
         return GEvent.getBool(this.order[var1 + 1]);
      }

      public boolean getBoolEX(int var1, int var2) {
         return GEvent.getBoolEX(this.order[var1 + 1], this.order[var2 + 1]);
      }

      public String getCommand() {
         return this.order[0];
      }

      public float getFloat(int var1) {
         return Float.parseFloat(this.order[var1 + 1]);
      }

      public int getInt(int var1) {
         return Integer.parseInt(this.order[var1 + 1]);
      }

      public String getString(int var1) {
         return GEvent.getString(this.order[var1 + 1]);
      }

      public void init() {
         this.status = 0;
      }

      public String toString() {
         String var2 = "";

         for(int var1 = 0; var1 < this.order.length; ++var1) {
            var2 = var2 + this.order[var1] + ",";
         }

         return var2;
      }
   }
}
