package ze.core.script;

public class GEvent_Simple extends GEvent {
   private int curLine = 0;
   private int status = 0;

   private int getEndIfIndex() {
      int var3 = 0;
      int var1 = this.curLine + 1;

      int var2;
      while(true) {
         if(var1 >= this.getOrderSize()) {
            var2 = -1;
            break;
         }

         String var4 = this.getOrder(var1).getCommand();
         if(!var4.equals("if") && !var4.equals("if_ex")) {
            if(var4.equals("endIf")) {
               var2 = var1;
               if(var3 == 0) {
                  break;
               }

               var2 = var3 - 1;
            } else {
               var2 = var3;
               if(var4.equals("else")) {
                  var2 = var3;
                  if(var3 == 0) {
                     return var1;
                  }
               }
            }
         } else {
            var2 = var3 + 1;
         }

         ++var1;
         var3 = var2;
      }

      return var2;
   }

   private int getEndLoopIndex() {
      int var3 = 0;
      int var1 = this.curLine + 1;

      int var2;
      while(true) {
         if(var1 >= this.getOrderSize()) {
            var2 = -1;
            break;
         }

         String var4 = this.getOrder(var1).getCommand();
         if(!var4.equals("loop") && !var4.equals("loop_ex")) {
            var2 = var3;
            if(var4.equals("endLoop")) {
               var2 = var1;
               if(var3 == 0) {
                  break;
               }

               var2 = var3 - 1;
            }
         } else {
            var2 = var3 + 1;
         }

         ++var1;
         var3 = var2;
      }

      return var2;
   }

   private int getLoopIndex() {
      int var2 = 0;

      int var3;
      for(int var1 = this.curLine + 1; var1 < this.getOrderSize(); var2 = var3) {
         String var4 = this.getOrder(var1).getCommand();
         if(!var4.equals("loop") && !var4.equals("loop_ex")) {
            var3 = var2;
            if(var4.equals("endLoop")) {
               var3 = var2 + 1;
            }
         } else {
            var3 = var2;
            if(var2 == 0) {
               return var1 - 1;
            }
         }

         ++var1;
      }

      return -1;
   }

   private GEvent.SOrder getOrder(int var1) {
      return (GEvent.SOrder)this.root.getOrder(var1);
   }

   private int getOrderSize() {
      return this.root.getSize();
   }

   public int getStatus() {
      return this.status;
   }

   protected void init(GScript var1, GEvent.Data var2) {
      this.script = var1;
      this.data = var2;
      String[][] var5 = var2.getOrders();
      int var4 = var5.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         String[] var6 = var5[var3];
         this.root.addOrder(0, var6);
      }

   }

   public void parse(Object var1, GEvent.SOrder var2) {
      String var4 = var2.getCommand();
      boolean var3;
      if(!var4.equals("loop") && !var4.equals("loop_ex")) {
         if(!var4.equals("if") && !var4.equals("if_ex")) {
            if(var4.equals("else")) {
               this.curLine = this.getEndIfIndex();
               return;
            }

            if(!var4.equals("endIf")) {
               if(var4.equals("endLoop")) {
                  this.curLine = this.getLoopIndex();
                  return;
               }

               if(var4.equals("break")) {
                  this.curLine = this.getOrderSize();
                  this.setEnd(var2);
                  return;
               }

               if(var4.equals("call")) {
                  this.script.run(var1, var2.getString(0));
                  return;
               }

               if(var4.equals("setResult")) {
                  this.setEnd(var2);
                  return;
               }

               if(var4.equals("setInt")) {
                  GParser.setInt(var2.getString(0));
                  return;
               }

               if(var4.equals("setBool")) {
                  GParser.setBool(var2.getString(0));
                  return;
               }

               if(var4.equals("setString")) {
                  GParser.setString(var2.getString(0));
                  return;
               }

               this.script.parse(var1, var2);
               return;
            }
         } else {
            if(var4.equals("loop")) {
               var3 = var2.getBool(0);
            } else {
               var3 = var2.getBoolEX(0, 1);
            }

            if(!var3) {
               this.curLine = this.getEndIfIndex();
               return;
            }
         }
      } else {
         if(var4.equals("loop")) {
            var3 = var2.getBool(0);
         } else {
            var3 = var2.getBoolEX(0, 1);
         }

         if(!var3) {
            this.curLine = this.getEndLoopIndex();
         }
      }

   }

   public void reset() {
      this.curLine = 0;
   }

   public void run(Object var1) {
      int var2 = this.getOrderSize();
      this.result = "";

      for(this.curLine = 0; this.curLine < var2; ++this.curLine) {
         this.parse(var1, this.getOrder(this.curLine));
      }

      this.setStatus(2);
   }

   public void setEnd(GEvent.SOrder var1) {
      this.result = var1.getString(0);
   }

   public void setStatus(int var1) {
      this.status = var1;
   }
}
