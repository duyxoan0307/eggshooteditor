package ze.core.script;

import java.util.HashMap;

public class GEvent_Tree extends GEvent {
   public static final int BLOCK_STEP = 1;
   public static final int BLOCK_SYNC = 0;
   private static HashMap proxyList = new HashMap();
   boolean isContinue = false;
   private Object user;

   public static void finishProxy(Object var0) {
      Object[] var1 = (Object[])proxyList.get(var0);
      if(var1 != null) {
         ((GEvent)var1[0]).setEnd((GEvent.SOrder)var1[1]);
         proxyList.remove(var0);
      }

   }

   private String getCommand(String[] var1) {
      return var1[0];
   }

   private int getElseIndex(GEvent.SNode var1) {
      for(int var2 = 0; var2 < var1.getSize(); ++var2) {
         if(!var1.isNode(var2) && ((GEvent.SOrder)var1.getOrder(var2)).getCommand().equals("else")) {
            return var2;
         }
      }

      return -1;
   }

   private int getEndIndex(String[][] var1, int var2, String var3, String var4) {
      int var6 = 0;

      for(int var5 = var2; var5 < var1.length; var6 = var2) {
         String var7 = this.getCommand(var1[var5]);
         if(var7.equals(var3)) {
            var2 = var6 + 1;
         } else {
            var2 = var6;
            if(var7.equals(var4)) {
               --var6;
               var2 = var6;
               if(var6 == 0) {
                  return var5;
               }
            }
         }

         ++var5;
      }

      return -1;
   }

   private int getOrderEnd(GEvent.SNode var1) {
      byte var4 = 0;
      byte var2;
      switch(var1.type) {
      case 0:
         for(int var3 = 0; var3 < var1.getSize(); ++var3) {
            var2 = var4;
            if(var1.getOrder(var3).status != 2) {
               return var2;
            }
         }

         var1.status = 2;
         if(var1.parent != null) {
            return this.getOrderEnd(var1.parent);
         }
         break;
      case 1:
         if(var1.curLine != var1.lastLine()) {
            ++var1.curLine;
            return 0;
         }

         var1.status = 2;
         if(var1.parent != null) {
            return this.getOrderEnd(var1.parent);
         }
      }

      var2 = 2;
      return var2;
   }

   private void initNode(GEvent.SNode var1) {
      var1.init();
   }

   private GEvent.SNode initTree(String[][] var1, int var2, int var3, GEvent.SNode var4, int var5) {
      var4 = new GEvent.SNode(var4);
      var4.type = var5;
      var4.status = 0;
      var4.curLine = 0;
      if(var2 > 0) {
         var4.addOrder(0, var1[var2 - 1]);
      }

      for(; var2 < var3; ++var2) {
         String var6 = var1[var2][0];
         if(var6.equals("{")) {
            var5 = this.getEndIndex(var1, var2, "{", "}");
            var4.addOrder(this.initTree(var1, var2 + 1, var5 + 1, var4, 1));
            var2 = var5;
         } else if(var6.equals("if")) {
            var5 = this.getEndIndex(var1, var2, "if", "endIf");
            var4.addOrder(this.initTree(var1, var2 + 1, var5 + 1, var4, 1));
            var2 = var5;
         } else if(var6.equals("loop")) {
            var5 = this.getEndIndex(var1, var2, "loop", "endLoop");
            var4.addOrder(this.initTree(var1, var2 + 1, var5 + 1, var4, 1));
            var2 = var5;
         } else if(var6.equals("sync")) {
            var5 = this.getEndIndex(var1, var2, "sync", "endSync");
            var4.addOrder(this.initTree(var1, var2 + 1, var5 + 1, var4, 0));
            var2 = var5;
         } else {
            var4.addOrder(0, var1[var2]);
         }
      }

      return var4;
   }

   private void parseOrder(GEvent.SOrder var1) {
      if(var1.status == 0) {
         var1.status = 1;
         this.script.parse(this, var1);
         if(this.user == null) {
            this.setEnd(var1);
         } else {
            sender(this.user, this, var1);
         }

         if(this.isContinue) {
            this.run((Object)null);
         }

         this.isContinue = false;
         this.user = null;
      }
   }

   private void runNode(GEvent.SNode var1) {
      int var2 = 0;
      if(!var1.isEmpty() && var1.status != 2) {
         switch(var1.type) {
         case 0:
            for(; var2 < var1.getSize(); ++var2) {
               if(var1.isNode(var2)) {
                  this.runNode((GEvent.SNode)var1.getOrder(var2));
               } else {
                  this.parseOrder((GEvent.SOrder)var1.getOrder(var2));
               }
            }
            break;
         case 1:
            var2 = var1.curLine;
            if(var1.isNode(var2)) {
               this.runNode((GEvent.SNode)var1.getOrder(var2));
               return;
            }

            GEvent.SOrder var4 = (GEvent.SOrder)var1.getOrder(var2);
            String var5 = var4.getCommand();
            boolean var3;
            if(!var5.equals("loop") && !var5.equals("loop_ex")) {
               if(!var5.equals("if") && !var5.equals("if_ex")) {
                  if(!var5.equals("sync") && !var5.equals("{")) {
                     if(var5.equals("else")) {
                        var1.curLine = var1.lastLine();
                        this.setEnd(var4);
                        this.run((Object)null);
                        return;
                     }

                     if(!var5.equals("endSync") && !var5.equals("endIf") && !var5.equals("}")) {
                        if(var5.equals("endLoop")) {
                           var1.curLine = 0;
                           this.runNode(var1);
                           return;
                        }

                        if(var5.equals("setResult")) {
                           this.result = var4.getString(0);
                           this.setEnd(var4);
                           this.run((Object)null);
                           return;
                        }

                        if(var5.equals("setInt")) {
                           GParser.setInt(var4.getString(0));
                           this.setEnd(var4);
                           this.run((Object)null);
                           return;
                        }

                        if(var5.equals("setBool")) {
                           GParser.setInt(var4.getString(0));
                           this.setEnd(var4);
                           this.run((Object)null);
                           return;
                        }

                        if(var5.equals("setString")) {
                           GParser.setInt(var4.getString(0));
                           this.setEnd(var4);
                           this.run((Object)null);
                           return;
                        }

                        if(var5.equals("break")) {
                           this.result = var4.getString(0);
                           this.root.status = 2;
                           return;
                        }

                        this.parseOrder(var4);
                        return;
                     }

                     this.setEnd(var4);
                     this.run((Object)null);
                     return;
                  }

                  ++var1.curLine;
                  this.runNode(var1);
                  return;
               }

               if(var5.equals("if")) {
                  var3 = var4.getBool(0);
               } else {
                  var3 = var4.getBoolEX(0, 1);
               }

               if(var3) {
                  ++var2;
               } else {
                  var2 = this.getElseIndex(var1);
                  if(var2 == -1) {
                     var1.curLine = var1.lastLine();
                     this.setEnd(var4);
                     return;
                  }

                  ++var2;
               }

               var1.curLine = var2;
               this.runNode(var1);
               return;
            }

            if(var5.equals("loop")) {
               var3 = var4.getBool(0);
            } else {
               var3 = var4.getBoolEX(0, 1);
            }

            if(var3) {
               this.initNode(var1);
               var1.curLine = var2 + 1;
               this.runNode(var1);
               return;
            }

            var1.curLine = var1.lastLine();
            this.setEnd(var4);
            return;
         default:
            return;
         }
      }

   }

   static void sender(Object var0, GEvent var1, GEvent.SOrder var2) {
      proxyList.put(var0, new Object[]{var1, var2});
   }

   public static boolean setProxy(Object var0, Object var1) {
      if(var0 != null && var1 != null && var0.getClass() == GEvent_Tree.class) {
         ((GEvent_Tree)var0).user = var1;
         return false;
      } else {
         return true;
      }
   }

   public void continueRun(Object var1) {
      if(var1 != null && var1.getClass() == GEvent_Tree.class) {
         ((GEvent_Tree)var1).isContinue = true;
      }
   }

   public int getStatus() {
      return this.root.status;
   }

   public void init(GScript var1, GEvent.Data var2) {
      this.script = var1;
      this.data = var2;
      this.root = this.initTree(var2.getOrders(), 0, var2.getOrderSize(), this.root, 1);
   }

   public void reset() {
      this.initNode(this.root);
   }

   public void run(Object var1) {
      this.runNode(this.root);
   }

   public void setEnd(GEvent.SOrder var1) {
      var1.status = 2;
      this.getOrderEnd(var1.parent);
   }

   public void setStatus(int var1) {
      this.root.status = var1;
   }
}
