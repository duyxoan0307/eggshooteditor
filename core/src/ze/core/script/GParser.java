package ze.core.script;

import ze.core.util.GTools;

import java.util.regex.Pattern;

public abstract class GParser {
   private static boolean isNot = false;
   private static boolean minus = false;
   private static final Pattern p_bool = Pattern.compile("[@&><=!()]");
   private static final Pattern p_int = Pattern.compile("[^()+-/*///%]");

   private static String[] checkArray(String var0) {
      int var1 = var0.indexOf("[");
      return var1 == -1?null:new String[]{var0.substring(0, var1), var0.substring(var1 + 1, var0.indexOf("]"))};
   }

   private static int getArrayValue(String var0, int var1) {
      GVariable var2 = getVariable();
      if(var2 == null) {
         System.err.println("get variable instance is null  (getArrayValue)");
         return Integer.MAX_VALUE;
      } else {
         return var2.getArray(var0, var1);
      }
   }

   public static boolean getBool(String var0) {
      var0 = trimString(var0);
      return isExpression(var0, Boolean.TYPE)?parseBoolExpression(var0):parseBool(var0);
   }

   public static boolean getBoolEX(int var0, short[] var1) {
      GVariable var2 = getVariable();
      if(var2 == null) {
         System.err.println("get variable instance is null  (getBoolEX)");
         return false;
      } else {
         return var2.getBoolEX(var0, var1);
      }
   }

   private static boolean getCompValue(String var0) {
      int var1 = 0;

      int var2;
      for(var2 = -1; var1 < var0.length(); ++var1) {
         char var3 = var0.charAt(var1);
         if(var3 == 62 || var3 == 60 || var3 == 61 || var3 == 126) {
            var2 = var1;
         }
      }

      if(var2 == -1) {
         boolean var6;
         if(var0.indexOf(33) != -1) {
            var6 = true;
         } else {
            var6 = false;
         }

         if(!var6) {
            return parseBool(var0);
         }

         if(!parseBool(var0.substring(1, var0.length()))) {
            return true;
         }
      } else {
         String var4 = var0.substring(0, var2);
         String var5 = var0.substring(var2 + 1, var0.length());
         switch(var0.charAt(var2)) {
         case '<':
            if(parseMathExpression(var4) < parseMathExpression(var5)) {
               return true;
            }
            break;
         case '=':
            if(parseMathExpression(var4) == parseMathExpression(var5)) {
               return true;
            }
            break;
         case '>':
            if(parseMathExpression(var4) > parseMathExpression(var5)) {
               return true;
            }
            break;
         case '~':
            if(parseMathExpression(var4) != parseMathExpression(var5)) {
               return true;
            }
            break;
         default:
            return false;
         }
      }

      return false;
   }

   private static String[] getExpArray(String var0) {
      String[] var2 = GTools.splitString(var0, "=");
      String[] var1 = var2;
      if(var2.length < 2) {
         System.err.println("invalid expression (getExpArray) " + var0);
         var1 = null;
      }

      return var1;
   }

   private static int getFlag4Index(String var0) {
      int var1 = 0;
      int var3 = 0;

      while(true) {
         if(var1 >= var0.length()) {
            var3 = -1;
            break;
         }

         char var4 = var0.charAt(var1);
         int var2;
         if(var4 == 40) {
            var2 = var3 + 1;
         } else {
            var2 = var3;
            if(var4 == 41) {
               var2 = var3 - 1;
            }
         }

         if(var2 == 0) {
            var3 = var1;
            if(var4 == 43) {
               break;
            }

            var3 = var1;
            if(var4 == 45) {
               break;
            }

            var3 = var1;
            if(var4 == 42) {
               break;
            }

            var3 = var1;
            if(var4 == 47) {
               break;
            }

            var3 = var1;
            if(var4 == 37) {
               break;
            }
         }

         ++var1;
         var3 = var2;
      }

      return var3;
   }

   private static int getFlagIndex(String var0) {
      int var1 = 0;
      int var3 = 0;

      while(true) {
         if(var1 >= var0.length()) {
            var3 = -1;
            break;
         }

         char var4 = var0.charAt(var1);
         int var2;
         if(var4 == 40) {
            var2 = var3 + 1;
         } else {
            var2 = var3;
            if(var4 == 41) {
               var2 = var3 - 1;
            }
         }

         if(var1 > 0 && var2 == 0) {
            var3 = var1;
            if(var4 == 38) {
               break;
            }

            var3 = var1;
            if(var4 == 64) {
               break;
            }
         }

         ++var1;
         var3 = var2;
      }

      return var3;
   }

   public static int getInt(String var0) {
      var0 = trimString(var0);
      return isExpression(var0, Integer.TYPE)?parseMathExpression(var0):parseInt(var0);
   }

   public static String getString(String var0) {
      GVariable var1 = getVariable();
      if(var1 == null) {
         System.err.println("get variable instance is null  (getString)");
         return parseString(var0);
      } else {
         return parseString(var1.getString(var0));
      }
   }

   private static String getSubString(String var0, int var1, int var2) {
      boolean var6 = true;
      var0 = var0.substring(var1, var2);
      isNot = false;
      var1 = 0;

      for(var2 = 0; var1 < var0.length() && var0.charAt(var1) == 33 && var1 == var2; ++var1) {
         ++var2;
      }

      var0.charAt(var2);
      if(var0.charAt(var2) == 40) {
         int var3 = var2 + 1;

         for(int var4 = 1; var3 < var0.length(); var4 = var1) {
            char var5 = var0.charAt(var3);
            if(var5 == 40) {
               var1 = var4 + 1;
            } else {
               var1 = var4;
               if(var5 == 41) {
                  var1 = var4 - 1;
               }
            }

            if(var1 == 0 && var3 < var0.length() - 1) {
               return var0;
            }

            ++var3;
         }

         if(var2 % 2 == 0) {
            var6 = false;
         }

         isNot = var6;
         if(isNot) {
            return var0.substring(var2, var0.length());
         } else {
            return var0.substring(var2 + 1, var0.length() - 1);
         }
      } else {
         return var0;
      }
   }

   private static String getSubString_Var(String var0, int var1, int var2) {
      int var4 = 1;
      byte var3 = 0;
      var0 = var0.substring(var1, var2);
      minus = false;
      var2 = 0;

      for(var1 = var3; var1 < var0.length() && var0.charAt(var1) == 94 && var1 == var2; ++var1) {
         ++var2;
         minus = true;
      }

      if(var0.charAt(var2) != 40) {
         if(minus) {
            return var0.substring(var2, var0.length());
         } else {
            return var0;
         }
      } else {
         for(int var6 = var2 + 1; var6 < var0.length(); var4 = var1) {
            char var5 = var0.charAt(var6);
            if(var5 == 40) {
               var1 = var4 + 1;
            } else {
               var1 = var4;
               if(var5 == 41) {
                  var1 = var4 - 1;
               }
            }

            if(var1 == 0 && var6 < var0.length() - 1) {
               return var0;
            }

            ++var6;
         }

         if(minus) {
            return var0.substring(var2, var0.length());
         } else {
            return var0.substring(var2 + 1, var0.length() - 1);
         }
      }
   }

   private static GVariable getVariable() {
      return GVariable.getInstance();
   }

   private static boolean isExpression(String var0, Class var1) {
      return var1 == Integer.TYPE?p_int.matcher(var0).find():p_bool.matcher(var0).find();
   }

   private static boolean parseBool(String var0) {
      GVariable var1 = getVariable();
      if(var1 == null) {
         System.err.println("get variable instance is null  (parseBool)");
         return false;
      } else {
         return var1.getBool(var0);
      }
   }

   private static boolean parseBoolExpression(String var0) {
      boolean var3 = true;
      boolean var4 = false;
      var0 = getSubString(var0, 0, var0.length());
      boolean var5 = isNot;
      int var1 = getFlagIndex(var0);
      char var2;
      if(var1 == -1) {
         var1 = 0;

         boolean var7;
         while(true) {
            if(var1 >= var0.length()) {
               var7 = false;
               break;
            }

            var2 = var0.charAt(var1);
            if(var2 == 38 || var2 == 64 || var2 == 40) {
               var7 = true;
               break;
            }

            ++var1;
         }

         if(var7) {
            if(!var5) {
               return parseBoolExpression(var0);
            }

            if(!parseBoolExpression(var0)) {
               return var3;
            }
         } else {
            if(!var5) {
               return getCompValue(var0);
            }

            if(!getCompValue(var0)) {
               return true;
            }
         }

         var3 = false;
         return var3;
      } else {
         var5 = parseBoolExpression(var0.substring(0, var1));
         var2 = var0.charAt(var1);
         boolean var6 = parseBoolExpression(var0.substring(var1 + 1, var0.length()));
         switch(var2) {
         case '&':
            var3 = var4;
            if(var5) {
               var3 = var4;
               if(var6) {
                  var3 = true;
               }
            }
            break;
         case '@':
            if(!var5) {
               var3 = var4;
               if(!var6) {
                  break;
               }
            }

            var3 = true;
            break;
         default:
            var3 = var4;
         }

         return var3;
      }
   }

   protected static int parseInt(String var0) {
      var0 = var0.trim();

      try {
         int var1 = Integer.parseInt(var0);
         return var1;
      } catch (NumberFormatException var3) {
         String[] var2 = checkArray(var0);
         if(var2 != null) {
            return getArrayValue(var2[0], Integer.parseInt(var2[1]));
         } else {
            GVariable var4 = getVariable();
            if(var4 == null) {
               System.err.println("get variable instance is null  (parseInt)");
               return Integer.MAX_VALUE;
            } else {
               return var4.getInt(var0);
            }
         }
      }
   }

   private static int parseMathExpression(String var0) {
      boolean var2 = false;
      var0 = getSubString_Var(var0, 0, var0.length());
      boolean var4 = minus;
      int var1 = getFlag4Index(var0);
      char var3;
      if(var1 != -1) {
         int var6 = parseMathExpression(var0.substring(0, var1));
         var3 = var0.charAt(var1);
         var1 = parseMathExpression(var0.substring(var1 + 1, var0.length()));
         switch(var3) {
         case '%':
            return var6 % var1;
         case '&':
         case '\'':
         case '(':
         case ')':
         case ',':
         case '.':
         default:
            return 0;
         case '*':
            return var6 * var1;
         case '+':
            return var6 + var1;
         case '-':
            return var6 - var1;
         case '/':
            return var6 / var1;
         }
      } else {
         var1 = 0;

         boolean var5;
         while(true) {
            if(var1 >= var0.length()) {
               var5 = var2;
               break;
            }

            var3 = var0.charAt(var1);
            if(var3 == 43 || var3 == 45 || var3 == 42 || var3 == 47 || var3 == 37 || var3 == 94 || var3 == 40) {
               var5 = true;
               break;
            }

            ++var1;
         }

         return var5?(var4?-parseMathExpression(var0):parseMathExpression(var0)):(var4?-parseInt(var0):parseInt(var0));
      }
   }

   private static String parseString(String var0) {
      if(var0 == null) {
         return "";
      } else {
         StringBuffer var5 = new StringBuffer();
         StringBuffer var4 = new StringBuffer();
         boolean var2 = true;

         for(int var3 = 0; var3 < var0.length(); ++var3) {
            char var1 = var0.charAt(var3);
            if(var1 == 60) {
               var2 = false;
            } else if(var1 != 62) {
               if(var2) {
                  var5.append(var1);
               } else {
                  var4.append(var1);
               }
            } else {
               String[] var6 = GTools.splitString(var4.toString(), " ");
               if(var6[0].equals("int")) {
                  var5.append(parseInt(var6[1]));
               } else if(var6[0].equals("bool")) {
                  var5.append(getBool(var6[1]));
               } else if(!var6[0].equals("str") && !var6[0].equals("string")) {
                  System.err.println("无效的类型 ： " + var6[0]);
               } else {
                  var5.append(getString(var6[1]));
               }

               var4 = new StringBuffer();
               var2 = true;
            }
         }

         return var5.toString();
      }
   }

   public static void setBool(String var0) {
      String[] var4 = getExpArray(var0);
      if(var4 != null) {
         if(var4[1].indexOf(",") != -1) {
            String[] var2 = GTools.splitString(var4[1], ",");
            short[] var3 = new short[var2.length - 1];

            for(int var1 = 0; var1 < var3.length; ++var1) {
               var3[var1] = Short.parseShort(var2[var1 + 1]);
            }

            setBool(var4[0], getBoolEX(Integer.parseInt(var2[0]), var3));
            return;
         }

         setBool(var4[0], getBool(var4[1]));
      }

   }

   public static void setBool(String var0, boolean var1) {
      GVariable var2 = getVariable();
      if(var2 == null) {
         System.err.println("get variable instance is null  (setBool)");
      } else {
         var2.setBool(var0.trim(), var1);
      }
   }

   public static void setInt(String var0) {
      String[] var1 = getExpArray(var0);
      if(var1 != null) {
         setInt(var1[0], getInt(var1[1]));
      }

   }

   public static void setInt(String var0, int var1) {
      GVariable var2 = getVariable();
      if(var2 == null) {
         System.err.println("get variable instance is null  (setIntValue)");
      } else {
         String[] var3 = checkArray(var0);
         if(var3 == null) {
            var2.setInt(var0, var1);
         } else {
            var2.setArray(var3[0], Integer.parseInt(var3[1]), var1);
         }
      }
   }

   public static void setString(String var0) {
      String[] var1 = getExpArray(var0);
      if(var1 != null) {
         setString(var1[0], var1[1]);
      }

   }

   public static void setString(String var0, String var1) {
      GVariable var2 = getVariable();
      if(var2 == null) {
         System.err.println("get variable instance is null  (setString)");
      } else {
         var2.setString(var0.trim(), parseString(var1));
      }
   }

   public static String trimString(String var0) {
      return var0.replaceAll(" ", "");
   }

   protected abstract boolean checkIn(Object var1, GScript var2, GEvent var3);

   protected abstract void checkOut(Object var1, GScript var2, GEvent var3);

   protected abstract void parse(Object var1, GScript var2, GEvent.SOrder var3);
}
