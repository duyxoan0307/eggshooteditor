package ze.core.script;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;

import ze.core.util.GRes;

import java.util.Iterator;

public final class GScript {
   ObjectMap eventTable = new ObjectMap();
   private int eventType;
   private String packName;
   private GParser parser;

   public GScript(String var1, int var2, GParser var3) {
      this.eventType = var2;
      this.load(var1);
      this.parser = var3;
   }

   private void runEvent(Object var1, GEvent var2) {
      if(var2 != null && var2.getStatus() != 2) {
         if(var2.getStatus() == 0) {
            if(!this.parser.checkIn(var1, this, var2)) {
               return;
            }

            var2.setStatus(1);
         }

         if(var2.getStatus() == 1) {
            var2.run(var1);
            if(var2.getStatus() == 2) {
               this.parser.checkOut(var1, this, var2);
               if(var2.isAutoReset()) {
                  var2.reset();
                  return;
               }
            }
         }
      }

   }

   public boolean checkEventStatus(String var1, int var2) {
      GEvent var3 = this.getEvent(var1);
      return var3 != null && var3.getStatus() == var2;
   }

   public GEvent getEvent(String var1) {
      return (GEvent)this.eventTable.get(var1);
   }

   public int getEventSize() {
      return this.eventTable.size;
   }

   protected int getEventType() {
      return this.eventType;
   }

   public String getPackName() {
      return this.packName;
   }

   public void load(String var1) {
      JsonValue var7 = (new JsonReader()).parse(GRes.readTextFile(GRes.getScriptPath(var1)));

      for(int var2 = 0; var2 < var7.size; ++var2) {
         String var4 = var7.get(var2).name;
         //GMain.platform.log("key name : " + var4);
         if(var4.equals("event")) {
            JsonValue var8 = var7.get(var2);

            for(int var3 = 0; var3 < var8.size; ++var3) {
               GEvent.Data var5 = new GEvent.Data(var8.get(var3));
               GEvent var6 = GEvent.newInstance(this, var5);
               this.eventTable.put(var5.getHeader("eventID"), var6);
               //GMain.platform.log("put id : " + var5.getHeader("eventID"));
            }
         }
      }

   }

   protected void parse(Object var1, GEvent.SOrder var2) {
      this.parser.parse(var1, this, var2);
   }

   public void run(Object var1, String var2) {
      if(var2 != null) {
         this.runEvent(var1, this.getEvent(var2));
      } else {
         Iterator var3 = this.eventTable.values().iterator();

         while(var3.hasNext()) {
            this.runEvent(var1, (GEvent)var3.next());
         }

      }
   }

   public void setEventAutoReset(String var1, boolean var2) {
      if(var1 == null) {
         Iterator var3 = this.eventTable.values().iterator();

         while(var3.hasNext()) {
            ((GEvent)var3.next()).setAutoReset(var2);
         }
      } else {
         GEvent var4 = this.getEvent(var1);
         if(var4 != null) {
            var4.setAutoReset(var2);
            return;
         }
      }

   }
}
