package ze.core.script;

public abstract class GVariable {
   private static GVariable me;

   protected GVariable() {
      if(me == null) {
         me = this;
      }

   }

   public static GVariable getInstance() {
      if(me == null) {
         System.err.println("************ get variable instance is null **************");
      }

      return me;
   }

   protected abstract int defineArray(String var1, int var2, int var3, boolean var4);

   protected abstract boolean defineBool(String var1, boolean var2, boolean var3);

   protected abstract boolean defineBoolEX(int var1, short[] var2);

   protected abstract float defineFloat(String var1, float var2, boolean var3);

   protected abstract int defineInt(String var1, int var2, boolean var3);

   protected abstract String defineString(String var1, String var2, boolean var3);

   int getArray(String var1, int var2) {
      return this.defineArray(var1, var2, 0, false);
   }

   boolean getBool(String var1) {
      var1 = var1.trim();
      return !var1.toLowerCase().equals("true") && !var1.toLowerCase().equals("false")?this.defineBool(var1, false, false):Boolean.parseBoolean(var1);
   }

   boolean getBoolEX(int var1, short[] var2) {
      return this.defineBoolEX(var1, var2);
   }

   float getFloat(String var1) {
      return this.defineFloat(var1, 0.0F, false);
   }

   int getInt(String var1) {
      return this.defineInt(var1, 0, false);
   }

   String getString(String var1) {
      return this.defineString(var1, (String)null, false);
   }

   void setArray(String var1, int var2, int var3) {
      this.defineArray(var1, var2, var3, true);
   }

   void setBool(String var1, boolean var2) {
      this.defineBool(var1, var2, true);
   }

   void setFloat(String var1, float var2) {
      this.defineFloat(var1, var2, true);
   }

   void setInt(String var1, int var2) {
      this.defineInt(var1, var2, true);
   }

   void setString(String var1, String var2) {
      this.defineString(var1, var2, true);
   }
}
