package ze.core.util;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.scenes.scene2d.Stage;

import ze.core.transitions.GTransition;

public abstract class GDirectedGame implements ApplicationListener {
   private static boolean isUseTransition = true;
   private SpriteBatch batch;
   private FrameBuffer currFbo;
   private GScreen currScreen;
   private boolean isTransitionEnd;
   private FrameBuffer nextFbo;
   private GTransition screenTransition;
   private float time;

   public void dispose() {
      if(this.currScreen != null) {
         this.currScreen.hide();
      }

      if(this.batch != null) {
         this.currFbo.dispose();
         this.currScreen = null;
         this.nextFbo.dispose();
         this.batch.dispose();
         this.batch = null;
      }

   }

   public boolean isTransitionEnd() {
      return this.isTransitionEnd;
   }

   public void pause() {
      if(this.currScreen != null) {
         this.currScreen.pause();
      }

   }

   public void render() {
      if(!this.isTransitionEnd && isUseTransition && this.screenTransition != null) {
         float var1 = Math.min(GStage.getDelta(), GStage.getSleepTime());
         float var2 = this.screenTransition.duration;
         this.time = Math.min(this.time + var1, var2);
         if(this.time >= var2) {
            this.screenTransition = null;
            this.isTransitionEnd = true;
         } else {
            this.nextFbo.begin();
            this.currScreen.render(var1);
            this.nextFbo.end();
            var1 = this.time / var2;
            this.screenTransition.render(this.batch, this.currFbo.getColorBufferTexture(), this.nextFbo.getColorBufferTexture(), var1);
         }
      } else {
         this.currScreen.render(0.0F);
      }
   }

   public void resize(int width, int height) {
      Gdx.app.log("resize " , "width " + width + " " + height);
      GStage.getStage().getViewport().update(width, height);
      if(this.currScreen != null) {
         this.currScreen.resize(width, height);
      }

   }

   public void resume() {
      if(this.currScreen != null) {
         this.currScreen.resume();
      }

   }

   public void setScreen(GScreen var1) {
      this.setScreen(var1, (GTransition)null);
   }

   public void setScreen(GScreen var1, GTransition var2) {
      System.gc();
      var1.setGameInstance(this);
      if(isUseTransition && var2 != null) {
         int var3 =  Gdx.graphics.getWidth();
         int var4 =  Gdx.graphics.getHeight();
         this.isTransitionEnd = false;
         if(this.batch == null) {
            this.batch = new SpriteBatch(2);
            this.currFbo = new FrameBuffer(Pixmap.Format.RGB565, var3, var4, false);
            this.nextFbo = new FrameBuffer(Pixmap.Format.RGB565, var3, var4, false);
           // this.currFbo =  FrameBuffer.createFrameBuffer(Pixmap.Format.RGB565, var3, var4, false);
           // this.nextFbo =  FrameBuffer.createFrameBuffer(Pixmap.Format.RGB565, var3, var4, false);
         }

         if(this.currScreen != null) {
            this.currFbo.begin();
            this.currScreen.render(0.0F);
            this.currFbo.end();
            this.currScreen.hide();
         }

         this.currScreen = var1;
         this.currScreen.show();
        // this.currScreen.resize(GScreen.viewportW, GScreen.viewportH);
         this.screenTransition = var2;
         this.time = 0.0F;
      } else {
         if(this.currScreen != null) {
            this.currScreen.hide();
         }

         this.currScreen = var1;
         this.currScreen.show();
         this.isTransitionEnd = true;
      }
   }

   public void setUseTransition(boolean var1) {
      isUseTransition = var1;
   }
}
