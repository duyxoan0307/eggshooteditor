package ze.core.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import java.util.Comparator;

public enum GLayer {
	bottom("bottom", Touchable.childrenOnly),
	sprite("sprite", Touchable.childrenOnly),
	effect( "effect", Touchable.childrenOnly),
	ui( "ui", Touchable.childrenOnly),
	top("top", Touchable.childrenOnly);
		 

   private Comparator comparator;
   private GLayerGroup group;
   private String name;
   private Touchable touchable;


   private GLayer(String var3, Touchable var4) {
      this.name = var3;
      this.touchable = var4;
   }

   private void createComparator() {
      Comparator<Actor> var1 = new Comparator<Actor>() {
         public int compare(Actor var1, Actor var2) {
            return var1.getY() < var2.getY()?-1:(var1.getY() > var2.getY()?1:0);
         }
      };
      switch(this) {
      case bottom:
      case sprite:
      case effect:
      case ui:
      case top:
         var1 = null;
      default:
         this.comparator = var1;
      }
   }

   public Comparator getComparator() {
      return this.comparator;
   }

   public GLayerGroup getGroup() {
      return this.group;
   }

   public String getName() {
      return this.name;
   }

   public Touchable getTouchable() {
      return this.touchable;
   }

   public void init(GLayerGroup var1) {
      this.group = var1;
      var1.setName(this.name);
      var1.setTouchable(this.touchable);
      this.createComparator();
   }
}
