package ze.core.util;

import com.badlogic.gdx.Gdx;

import java.io.File;
import java.io.IOException;

public class GSaveAsFile {
   private static final String FILENAME = "MissionsData";

   private static File getFile(String var0) {
      File var2 = new File(Gdx.files.getLocalStoragePath() + var0);
      if(!var2.exists()) {
         try {
            var2.createNewFile();
            //GMain.platform.log("ok");
            return var2;
         } catch (IOException var1) {
            //GMain.platform.log("fail");
            var1.printStackTrace();
            return null;
         }
      } else {
         //GMain.platform.log("\u5b58\u5728");
         return var2;
      }
   }

   public static void readFile() {
      // $FF: Couldn't be decompiled
   }

   public static void saveFile() {
      // $FF: Couldn't be decompiled
   }
}
