package ze.core.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class GUI {
   public static Button creatButton(TextureRegion... var0) {
      TextureRegionDrawable[] var2 = new TextureRegionDrawable[4];
      if(var0 != null) {
         for(int var1 = 0; var1 < Math.min(4, var0.length); ++var1) {
            var2[var1] = new TextureRegionDrawable(var0[var1]);
            if(var1 == 0) {
               TextureRegionDrawable var3 = var2[0];
               var2[3] = var3;
               var2[2] = var3;
               var2[1] = var3;
            }
         }
      }

      Button.ButtonStyle var4 = new Button.ButtonStyle();
      var4.up = var2[0];
      var4.down = var2[1];
      var4.checked = var2[2];
      var4.disabled = var2[3];
      final Button btn = new Button(var4);

      btn.addListener(new ClickListener(){
         @Override
         public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            GSound.playSound("click.mp3");
         }

         public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
            btn.setColor(Color.GRAY);
            return super.touchDown(event, x, y, pointer, button);
         }

         public void touchUp (InputEvent event, float x, float y, int pointer, int button){
            btn.setColor(Color.WHITE);
            super.touchUp(event, x, y, pointer, button);
         }
      });
      return btn;
   }

   public static Button creatButton(Group parent, float x, float y, int align, TextureRegion... var0){
      Button r = creatButton(var0);
      parent.addActor(r);
      r.setPosition(x, y, align);
      return r;
   }
   public static Label creatLabel(CharSequence var0, Color var1) {
      return new Label(var0, new Label.LabelStyle(new BitmapFont(true), var1));
   }
}
