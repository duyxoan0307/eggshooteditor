package ze.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.utils.Align;

public class PositionRotationArc {
   private float degree;
   private float endDegree;
   private float endX;
   private float endY;
   private float originX;
   private float originY;
   private float passX;
   private float passY;
   private float radius;
   private float startDegree;
   private float startX;
   private float startY;

   public PositionRotationArc(float startx, float starty, float posx, float posy, float passx, float passy) {
      setPosition(posx, posy);
      setPass(passx, passy);
      this.startX = startx;
      this.startY = starty;
      this.calculateOrigin();
      this.calculateRadius();
      this.calculateDegree();
   }

   protected void begin() {

   }

   public void calculateDegree() {
      this.startDegree = (float)(Math.acos((double)((this.startX - this.originX) / this.radius)) * 180.0D / 3.141592653589793D);
      if(this.startY > this.originY) {
         this.startDegree = 360.0F - this.startDegree;
      }

      this.endDegree = (float)(Math.acos((double)((this.endX - this.originX) / this.radius)) * 180.0D / 3.141592653589793D);
      if(this.endY > this.originY) {
         this.endDegree = 360.0F - this.endDegree;
      }

      float var2 = (float)(Math.acos((double)((this.passX - this.originX) / this.radius)) * 180.0D / 3.141592653589793D);
      float var1 = var2;
      if(this.passY > this.originY) {
         var1 = 360.0F - var2;
      }

      if(this.endDegree >= this.startDegree) {
         if(var1 >= this.startDegree && var1 <= this.endDegree) {
            this.degree = this.endDegree - this.startDegree;
         } else {
            this.degree = -(360.0F - this.endDegree + this.startDegree);
         }
      } else if(var1 >= this.endDegree && var1 <= this.startDegree) {
         this.degree = this.endDegree - this.startDegree;
      } else {
         this.degree = 360.0F - this.startDegree + this.endDegree;
      }
   }

   public void calculateOrigin() {
      float var1 = this.passX - this.startX;
      float var5 = this.endX - this.passX;
      float var2 = this.passX + this.startX;
      float var6 = this.endX;
      float var7 = this.passX;
      float var3 = this.passY - this.startY;
      float var8 = this.endY - this.passY;
      float var4 = this.passY + this.startY;
      this.originX = (((var6 + var7) * var5 + (this.endY + this.passY) * var8) * var3 - (var4 * var3 + var2 * var1) * var8) / ((var5 * var3 - var8 * var1) * 2.0F);
      var5 = -var1 * this.originX / var3;
      this.originY = (var1 * var2 + var4 * var3) / (2.0F * var3) + var5;
   }

   public void calculateRadius() {
      this.radius = (float)Math.sqrt((double)((this.originX - this.startX) * (this.originX - this.startX) + (this.originY - this.startY) * (this.originY - this.startY)));
   }

   public float getX() {
      return this.endX;
   }

   public float getY() {
      return this.endY;
   }

   public void setPass(float var1, float var2) {
      this.passX = var1;
      this.passY = var2;
   }

   public void setPosition(float var1, float var2) {
      this.endX = var1;
      this.endY = var2;
   }

   public void setX(float var1) {
      this.endX = var1;
   }

   public void setY(float var1) {
      this.endY = var1;
   }

//   protected void update(float dt) {
//      float var2 = this.originX;
//      float var3 = this.radius;
//      float var4 = MathUtils.cosDeg(this.startDegree + this.degree * dt);
//      float var5 = this.originY;
//      float var6 = this.radius;
//      Gdx.app.log("dt", dt+"");
//      dt = MathUtils.sinDeg(this.startDegree + this.degree * dt);
//      this.actor.setPosition(var2 + var3 * var4, var5 - var6 * dt);
//      Gdx.app.log("dt2", dt+"");
//   }


   public void setActorPosition(float dt, Actor actor){
      Gdx.app.log("dt", "" + dt);
      float var2 = this.originX;
      float var3 = this.radius;
      float var4 = MathUtils.cosDeg(this.startDegree + this.degree * dt);
      float var5 = this.originY;
      float var6 = this.radius;
      float degrees = this.startDegree + this.degree * dt;
      dt = MathUtils.sinDeg(this.startDegree + this.degree * dt);

      actor.setPosition(var2 + var3 * var4, var5 - var6 * dt);
      actor.setRotation(270-degrees);
   }

   public void setActor(float dt, Actor actor){

      float var4 = MathUtils.cosDeg(this.startDegree + this.degree * dt);
      float degrees =  this.startDegree + this.degree * dt;
      dt = MathUtils.sinDeg(this.startDegree + this.degree * dt);

      float x = this.originX + this.radius * var4;
      float y = this.originY - this.radius * dt;


      actor.setPosition(x, y);
      actor.setRotation(270 - degrees);
   }

   public void setActorWithDelta(float dt, float deltaRadius, Actor actor){
      float newradius = this.radius + deltaRadius;
      float var4 = MathUtils.cosDeg(this.startDegree + this.degree * dt);
      float degrees =  this.startDegree + this.degree * dt;
      dt = MathUtils.sinDeg(this.startDegree + this.degree * dt);

      float x = this.originX + newradius * var4;
      float y = this.originY - newradius * dt;


      actor.setPosition(x, y);
      actor.setRotation(270 - degrees);
   }

   public ParallelAction genAction(float dt, float duration){

      float var4 = MathUtils.cosDeg(this.startDegree + this.degree * dt);
      float degrees =  this.startDegree + this.degree * dt;
      dt = MathUtils.sinDeg(this.startDegree + this.degree * dt);

      float x = this.originX + this.radius * var4;
      float y = this.originY - this.radius * dt;


      MoveToAction movetoaction = Actions.moveTo( x, y, duration);
      RotateToAction rotateByAction = Actions.rotateTo(270-degrees, duration);
      return Actions.parallel(movetoaction, rotateByAction);
   }

   public ParallelAction genActionWithDeltaRadius(float dt, float duration, float deltaRadius){
      float newradius = this.radius + deltaRadius;
      float var4 = MathUtils.cosDeg(this.startDegree + this.degree * dt);
      float degrees =  this.startDegree + this.degree * dt;
      dt = MathUtils.sinDeg(this.startDegree + this.degree * dt);

      float x = this.originX + newradius * var4;
      float y = this.originY - newradius * dt;


      MoveToAction movetoaction = Actions.moveTo( x, y, duration);
      RotateToAction rotateByAction = Actions.rotateTo(270-degrees, duration);
      return Actions.parallel(movetoaction, rotateByAction);
   }



}
