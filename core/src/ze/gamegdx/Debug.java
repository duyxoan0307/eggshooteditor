package ze.gamegdx;

import com.badlogic.gdx.Gdx;

public class Debug {
    public static void Log(Object log){
        String s_log = log.toString();
        if (!Util.isWebGL())
            Gdx.app.log("log",s_log);
        else
            consolelog("log: "+s_log);
    }
    public static void Log(String tag, Object log){
        String s_log = log.toString();
        if (!Util.isWebGL())
            Gdx.app.log(tag,s_log);
        else
            consolelog(tag+": "+s_log);
    }

    private static native void consolelog(String log) /*-{
            $wnd.console.log(log);
        }-*/;
}
