package ze.gamegdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;

public class HTTPRequest {
    public static void sendRequest(final String url, final HTTP_RequestCallback callback){
        if (callback==null)
            return;
        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        final Net.HttpRequest httpRequest = requestBuilder.newRequest().method(Net.HttpMethods.GET).url(url).build();
        Gdx.net.sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                callback.OnSuccess(httpResponse.getResultAsString());
            }

            @Override
            public void failed(Throwable t) {
                callback.OnFailed();
            }

            @Override
            public void cancelled() {
                callback.OnCancelled();
            }
        });
    }
    public interface HTTP_RequestCallback{
        public void OnSuccess(String data);
        public void OnFailed();
        public void OnCancelled();
    }
}