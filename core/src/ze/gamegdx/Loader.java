package ze.gamegdx;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

import ze.core.util.GAssetsManager;
import ze.core.util.GSound;
import ze.gamelogic.other.GameSettings;

public class Loader {
    public static Loader instance;
    public static BitmapFont font_white;
    public Loader(){
        if (instance!=null)
            return;
        instance=this;
        loadFonts();
        loadSounds();
    }
    private void loadSounds() {
        GSound.setSoundSilence(!GameSettings.isSound);
        GSound.setMusicSilence(!GameSettings.isMusic);
        GSound.initMusic("bgmusic.mp3");
        GSound.playMusic();
        GSound.initSound("click.mp3");
    }

    private void loadFonts() {
        font_white=GAssetsManager.getBitmapFont("font_white.fnt");
    }
}
