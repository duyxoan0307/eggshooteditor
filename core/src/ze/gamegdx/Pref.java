package ze.gamegdx;

import com.badlogic.gdx.Preferences;

import ze.core.util.GTools;

public class Pref {
    public static String pref_name = "gameData";
    public static Preferences pref=GTools.getPreferences(pref_name);
    public static void putInteger(String key,int value){
        pref.putInteger(key,value);
        pref.flush();
    }
    public static void putString(String key,String value){
        pref.putString(key,value);
        pref.flush();
    }
    public static void putBoolean(String key,boolean value){
        pref.putBoolean(key,value);
        pref.flush();
    }
    public static void putLong(String key,long value){
        pref.putLong(key,value);
        pref.flush();
    }
    public static int getInteger(String key,int default_value){
        return pref.getInteger(key,default_value);
    }
    public static String getString(String key,String default_value){
        return pref.getString(key,default_value);
    }
    public static boolean getBoolean(String key,boolean default_value){
        return pref.getBoolean(key,default_value);
    }
    public static long getLong(String key,long default_value){
        return pref.getLong(key,default_value);
    }
    public static void clear(){
        pref.clear();
        pref.flush();
    }
}
