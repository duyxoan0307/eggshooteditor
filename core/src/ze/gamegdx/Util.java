package ze.gamegdx;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import java.nio.ByteBuffer;

import ze.GMain;
import ze.core.action.exAction.GSimpleAction;

public class Util {
    public static boolean isWebGL(){
        return Gdx.app.getType()==Application.ApplicationType.WebGL;
    }
    public static TextureAtlas.AtlasRegion findRegion(TextureAtlas textureAtlas, String name){
        TextureAtlas.AtlasRegion atlasRegion;
        atlasRegion=textureAtlas.findRegion(name);
        if (atlasRegion==null)
        {
            Gdx.app.error("Error"," texture atlas "+ textureAtlas+": "+ name +" null!",new Exception(""));
        }
        return atlasRegion;
    }
    public static void delayFunction(final OndelayCallback callback, float second, Group group){
        group.addAction(Actions.sequence(Actions.delay(second),GSimpleAction.simpleAction(new GSimpleAction.ActInterface() {
            @Override
            public boolean act(float dt, Actor actor) {
                //maybe postRunable
                callback.OnEvent();
                return true;
            }
        })));
    }
    public static float lerp(float point1, float point2, float alpha)
    {
        return point1 + alpha * (point2 - point1);
    }

    //callback
    public interface OndelayCallback{
        public void OnEvent();
    }
    public static Pixmap getScreenshot(){
        return GMain.getPlatform().getFrameBufferPixmap(0,0,Gdx.graphics.getBackBufferWidth(),Gdx.graphics.getBackBufferHeight());
    }
}