package ze.gamegdx.gui.gui_extra;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import ze.gamegdx.gui.tools.GUITool;

public class TextButton extends Group {
    public Image bg_img;
    public Label title_label;
    public TextButton(Image bg, Label title){
        this.bg_img=bg;
        this.title_label=title;

        this.setSize(bg.getWidth(),bg.getHeight());
        addActor(bg_img);
        addActor(title_label);
        title_label.setSize(getWidth(),getHeight());
        GUITool.fitLabel(title_label,1);

        this.title_label.setTouchable(Touchable.disabled);
    }
}
