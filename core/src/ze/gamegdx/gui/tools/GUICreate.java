package ze.gamegdx.gui.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import ze.gamegdx.ClickEvent;
import ze.gamegdx.gui.gui_extra.TextButton;

public class GUICreate {
    //button
    public static Button creatButton(TextureRegion... var0) {
        TextureRegionDrawable[] var2 = new TextureRegionDrawable[4];
        if (var0 != null) {
            for (int var1 = 0; var1 < Math.min(4, var0.length); ++var1) {
                var2[var1] = new TextureRegionDrawable(var0[var1]);
                if (var1 == 0) {
                    TextureRegionDrawable var3 = var2[0];
                    var2[3] = var3;
                    var2[2] = var3;
                    var2[1] = var3;
                }
            }
        }
        Button.ButtonStyle var4 = new Button.ButtonStyle();
        var4.up = var2[0];
        var4.down = var2[1];
        var4.checked = var2[2];
        var4.disabled = var2[3];
        final Button btn = new Button(var4);
        btn.setTransform(true);
        btn.setOrigin(Align.center);

        addActionPressedButton(btn);
        return btn;
    }
    //textbutton
    public static TextButton createTextButton(TextureRegion textureRegion, String title, BitmapFont font){
        Image bg_img = new Image(textureRegion);
        Label label = createLabel(title,font);
        TextButton btn = new TextButton(bg_img,label);
        addActionPressedButton(bg_img);
        return btn;
    }
    //label
    public static Label createLabel(String text, BitmapFont font){
        Label label=new Label(text,new Label.LabelStyle(font,Color.WHITE));
        label.setAlignment(Align.center);
        return label;
    }
    //texture region white
    public static TextureRegion createTextureRegion(int width, int height)
    {
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fillRectangle(0, 0, width, height);
        Texture texture = new Texture(pixmap);
        pixmap.dispose();

        return new TextureRegion(texture);
    }
    static void addActionPressedButton(final Actor btn){
        btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
//                GSound.playSound("click.mp3");
            }

            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
                btn.setColor(Color.GRAY);
                return super.touchDown(event, x, y, pointer, button);
            }

            public void touchUp (InputEvent event, float x, float y, int pointer, int button){
                btn.setColor(Color.WHITE);
                super.touchUp(event, x, y, pointer, button);
            }
        });
    }
}
