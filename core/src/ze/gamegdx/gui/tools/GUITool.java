package ze.gamegdx.gui.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import ze.core.exSprite.GShapeSprite;
import ze.core.util.GStage;

public class GUITool {
    public static void fitLabel(Label text, float max_size){
        scaleLabel(text,max_size);
        if (text.getWidth()<text.getPrefWidth())
        {
            scaleLabel(text, text.getFontScaleX() * text.getWidth()/text.getPrefWidth());
        }
    }
    public static void setTextAndFit(Label label,String text,float max_size){
        label.setText(text);
        fitLabel(label,max_size);
    }
    public static void scaleLabel(Label label, float font_size){
        label.setFontScale(font_size);
    }
    public static Color newColor(float r, float g, float b, float a){
        return new Color(r/255f,g/255f,b/255f,a);
    }
    public static Color newColor (String hex) {
        hex = hex.charAt(0) == '#' ? hex.substring(1) : hex;
        int r = Integer.valueOf(hex.substring(0, 2), 16);
        int g = Integer.valueOf(hex.substring(2, 4), 16);
        int b = Integer.valueOf(hex.substring(4, 6), 16);
        int a = hex.length() != 8 ? 255 : Integer.valueOf(hex.substring(6, 8), 16);
        return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
    }
    public static void resizeByScale(Actor actor, float scale)
    {
        actor.setSize(actor.getWidth()*scale,actor.getHeight()*scale);
    }
    public static void resizeFullScreen(Actor actor){
        actor.setSize(GStage.getWidth(),GStage.getHeight());
        actor.setPosition(0,0,Align.center);
    }
    public static void resizeWidthFullScreen(Actor actor){
        actor.setWidth(GStage.getWidth());
    }
    public static void resizeHeightFullScreen(Actor actor){
        actor.setWidth(GStage.getHeight());
    }

    public static void addActorNotChangePosition(Actor actor, Group parent){
        Vector2 stage_position = actor.localToStageCoordinates(new Vector2(actor.getX(Align.center),actor.getY(Align.center)));
        parent.addActor(actor);
        Vector2 local_position=actor.stageToLocalCoordinates(stage_position);
        actor.setPosition(local_position.x,local_position.y,Align.center);
    }

    public static void setTexture(Button myButton, Texture texture){
        TextureRegion tr = new TextureRegion(texture);
        TextureRegionDrawable td = new TextureRegionDrawable(tr);
        Button.ButtonStyle buttonStyle = new Button.ButtonStyle(td, td, td);
        myButton.setStyle(buttonStyle);
    }
    public static void setTexture(Image myImage, Texture texture){
        myImage.setDrawable(new TextureRegionDrawable(new TextureRegion(texture)));
    }
    public static GShapeSprite getOverlay(){
        GShapeSprite overlay = new GShapeSprite();
        overlay.createRectangle(true, -Gdx.graphics.getWidth()/2,  -Gdx.graphics.getHeight()/2, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        overlay.setColor(0.0F, 0.0F, 0.0F, 0.7F);
        overlay.setScale(3f);
        return overlay;
    }
}
