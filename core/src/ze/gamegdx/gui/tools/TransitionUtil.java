package ze.gamegdx.gui.tools;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import ze.core.util.GStage;

public class TransitionUtil {
    public static SequenceAction animationShowLeftToRight(){
        SequenceAction action = new SequenceAction();
        MoveToAction moveToAction = Actions.moveTo(-GStage.getWidth(),0);
        MoveToAction moveToAction1=Actions.moveTo(0,0,0.5f,Interpolation.bounceOut);
        action.addAction(moveToAction);
        action.addAction(moveToAction1);
        return action;
    }
    public static SequenceAction animationHideLeftToRight(){
        SequenceAction action = new SequenceAction();
        MoveToAction moveToAction = Actions.moveTo(GStage.getWidth(),0,0.5f,Interpolation.bounceIn);
        action.addAction(moveToAction);
        return action;
    }
    public static SequenceAction animationShowFadeIn(){
        SequenceAction action = new SequenceAction();
        action.addAction(Actions.fadeOut(0));
        action.addAction(Actions.fadeIn(0.5f));
        return action;
    }
    public static SequenceAction animationHideFadeOut(){
        SequenceAction action = new SequenceAction();
        action.addAction(Actions.fadeOut(0.5f));
        return action;
    }
    public static SequenceAction animationShowScale(){
        SequenceAction action = new SequenceAction();
        action.addAction(Actions.scaleTo(0,0));
        action.addAction(Actions.scaleTo(1,1,0.5f,Interpolation.bounceIn));
        return action;
    }
    public static SequenceAction animationHideScale(){
        SequenceAction action = new SequenceAction();
        action.addAction(Actions.scaleTo(1,1,0.5f,Interpolation.bounceOut));
        return action;
    }
}
