package ze.gamegdx.gui.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Align;

import ze.core.action.exAction.GSimpleAction;
import ze.core.exSprite.GShapeSprite;
import ze.core.util.GLayerGroup;

public abstract class UIGroup extends GLayerGroup {
    public GShapeSprite overlay;
    protected Group groupUI;
    SequenceAction animationShow,animationHide;
    public UIGroup(Group parent){
        parent.addActor(this);
        addOverlay();
        groupUI=new Group();
        super.addActor(groupUI);
        setAnimationShow(null);
        setAnimationHide(null);
        show();
    }
    final void addOverlay(){
        overlay=GUITool.getOverlay();
        overlay.setPosition(0,0,Align.center);
        overlay.setColor(Color.BLACK);
        overlay.addAction(Actions.alpha(0.7f));
        overlay.setVisible(false);

        super.addActor(overlay);
    }

    final protected void show(){
        overlay.setVisible(true);
        if (animationShow!=null){
            groupUI.addAction(animationShow);
        }
    }
    final protected void hide(){
        overlay.setVisible(false);
        if (animationHide!=null){
            animationHide.addAction(GSimpleAction.simpleAction(new GSimpleAction.ActInterface() {
                @Override
                public boolean act(float dt, Actor actor) {
                    UIGroup.this.remove();
                    return true;
                }
            }));
            groupUI.addAction(animationHide);
            return;
        }
        else
            this.remove();
    }

    final protected void setActiveOverlay(boolean is_active){
        if (is_active){
            super.addActor(overlay);
            overlay.toBack();
        }
        else
            removeActor(overlay);
    }
    protected void setAnimationShow(SequenceAction sequence_action){
        this.animationShow=sequence_action;
    }
    protected void setAnimationHide(SequenceAction sequence_action){
        this.animationHide=sequence_action;
    }

    @Override
    public void addActor(Actor actor) {
        groupUI.addActor(actor);
    }
}
