package ze.gamelogic.mvc.model;

import com.badlogic.gdx.utils.Array;

public class Level {
    public int index;
    public Array<int[]> rows;
    public float speed;
    int count_egg_type;
    public int time;
    public Level(int index, Array<int[]> rows,float speed,int time) {
        this.index=index;
        this.rows=rows;
        this.speed=speed;
        this.time=time;
        setCountEggType();
    }
    void setCountEggType(){
        Array<Integer> type = new Array<Integer>();
        for (int i=0;i<rows.size;i++){
            int[] row = rows.get(i);
            for (int j=0;j<row.length;j++){
                if (row[j]>-1 && !type.contains(row[j],false))
                {
                    type.add(row[j]);
                }
            }
        }
        this.count_egg_type=type.size;
    }
}
