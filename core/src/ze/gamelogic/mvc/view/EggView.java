package ze.gamelogic.mvc.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

import ze.gamegdx.ClickEvent;
import ze.gamegdx.Debug;
import ze.gamelogic.ui.EditorUI;

public class EggView extends Image {
    public int type;
    public EggView(TextureRegion textureRegion,int type){
        super(textureRegion);
        this.type=type;
        setEgg(type);
        this.addCaptureListener(new ActorGestureListener(){
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count==2){
                    setVisible();
                }else if(count==1){
                    if (button==0)
                        setEggChoosed();
                }
                super.tap(event, x, y, count, button);
            }
        });
        this.addListener(new ClickEvent(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                Debug.Log("left:",EditorUI.is_left_clicking);
                Debug.Log("right:",EditorUI.is_right_clicking);
                if (EditorUI.is_left_clicking){
                    setEggChoosed();
                }else if (EditorUI.is_right_clicking){
                    getColor().a=0;
                }
                super.enter(event, x, y, pointer, fromActor);
            }
        });
    }
    public void setEgg(int type){
        this.type=type;
        float alpha = getColor().a;
        if (type==-1)
            alpha=0;
        EditorUI.setEggType(this,this.type);
        getColor().a=alpha;
    }
    private void setEggChoosed() {
        float alpha = getColor().a;
        EditorUI.setEggType(this,EditorUI.egg_type_choosed);
        this.type=EditorUI.egg_type_choosed;
        getColor().a=alpha;
    }

    public void setVisible() {
        float alpha = getColor().a;
        if (alpha==0)
            getColor().a=1;
        else
            getColor().a=0;
    }
    public void setVisible(boolean is_visible) {
        if (is_visible)
            getColor().a=1;
        else
            getColor().a=0;
    }
    public int getType(){
        if (getColor().a==0)
            return -1;
        return type;
    }
}
