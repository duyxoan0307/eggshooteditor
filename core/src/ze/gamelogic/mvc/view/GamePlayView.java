package ze.gamelogic.mvc.view;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;

import ze.core.util.GLayerGroup;

public class GamePlayView extends GLayerGroup{
    public TextureAtlas gamePlayAtlas;
    public static GamePlayView instance;
    public GamePlayView(Group parent){
        instance=this;
        parent.addActor(this);
        initRes();
    }

    private void initRes() {
    }
}
