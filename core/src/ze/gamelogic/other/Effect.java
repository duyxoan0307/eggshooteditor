package ze.gamelogic.other;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

import ze.core.exSprite.particle.GParticleSprite;
import ze.core.exSprite.particle.GParticleSystem;
import ze.core.util.GAssetsManager;
import ze.gamegdx.Util;

public class Effect {
    public static Effect instance=new Effect();
    static String atlas_name = "particleatlas";
    static TextureAtlas effectatlas;
    static String[] effect_images = new String[]{};
    static String[] effect_particles=new String[]{"count_score.p","count_score_2.p","start_round.p"};

    public Effect(){
        instance=this;
        effectatlas=GAssetsManager.getTextureAtlas("effectatlas");
        initParticles();
    }
    void initParticles(){
        for (int i=0;i<effect_particles.length;i++){
            new GParticleSystem(effect_particles[i],atlas_name,1,1);
        }
    }

    public static ParticleEffect showEffectParticle(int i,Vector2 pos,Group parent){
        GParticleSprite particle = GParticleSystem.getGParticleSystem(effect_particles[i]).create(parent,pos.x,pos.y);
//        particle.getEffect().scaleEffect(scale);
        return particle.getEffect();
    }
    public static Actor showEffectImage(int i,Vector2 pos,Group parent){
        Image img = new Image(Util.findRegion(effectatlas,effect_images[i]));
        img.setPosition(pos.x,pos.y,Align.center);
        parent.addActor(img);
        return img;
    }
}
