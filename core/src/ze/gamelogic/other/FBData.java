package ze.gamelogic.other;

import com.badlogic.gdx.graphics.Texture;

import ze.GMain;
import ze.gamegdx.Pref;
import ze.gamelogic.ui.LoadingUI;
import ze.platform.IPlat;

public class FBData {
    public static String id;
    public static String name;
    public static Texture avatar;

    public static void syncData(){
        LoadingUI.showLoading();
        GMain.getPlatform().FBInstant_GetDoubleStats("money", new IPlat.FBInstant_GetStatsCallback() {
            @Override
            public void OnValue(double value, boolean undefined) {
                if (undefined){
                    value=Constant.DEFAULT_MONEY;
                }
                else if (value<Constant.MIN_MONEY){
                    value=Constant.MIN_MONEY;
                }
                Pref.putLong("money0",(long)value);
                updateChangeScore(0);
                LoadingUI.hideLoading();
            }
        });

        id = GMain.getPlatform().FBInstant_GetPlayerID();
        name = GMain.getPlatform().FBInstant_GetPlayerName();
        //update ui name
        String photo = GMain.getPlatform().FBInstant_GetPlayerPhoto();
        GMain.getPlatform().LoadUrlTexture(photo, new IPlat.UrlTextureCallback() {
            @Override
            public void success(Texture texture) {
                avatar=texture;
                //update ui avatar
            }

            @Override
            public void error() {

            }
        });
    }
    public static void askShortcut(){
        if (Pref.getBoolean("firstShowShortcut",false)==false)
            return;
        GMain.getPlatform().FBInstant_CanCreateShortCut(new IPlat.FBInstant_CanCreateCallback() {
            @Override
            public void OnCallback(boolean canCreateShortcut) {
                if(canCreateShortcut)
                    GMain.getPlatform().FBInstant_CreateShortCut();
                else
                    GMain.getPlatform().FBInstant_CanSubscribeBot();
                Pref.putBoolean("firstShowShortcut",true);
            }
        });
    }
    public static void updateChangeScore(int amount){
        GMain.getPlatform().FBInstant_IncrementDoubleStats("money", amount, new IPlat.FBInstant_IncrementDoubleStatsCallback() {
            @Override
            public void OnSuccess(double newvalue) {
                if (newvalue<Constant.MIN_MONEY)
                    newvalue=Constant.MIN_MONEY;
                GMain.getPlatform().FBInstant_SetDoubleStats("money",newvalue,null);
                GMain.getPlatform().FBInstant_ReportScore(newvalue);
                Pref.putLong("money0",(long)newvalue);
            }
        });
    }
}
