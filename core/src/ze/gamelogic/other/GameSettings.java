package ze.gamelogic.other;

import ze.gamegdx.Pref;

public class GameSettings {
    public static boolean isSound,isMusic;
    public static GameSettings instance=new GameSettings();
    public GameSettings(){
        isSound= Pref.getBoolean("isSound",true);
        isMusic= Pref.getBoolean("isMusic",true);
    }
    public static void setSound(boolean is){
        isSound=is;
        Pref.putBoolean("isSound",is);
    }
    public static void setMusic(boolean is){
        isMusic=is;
        Pref.putBoolean("isMusic",is);
    }
}
