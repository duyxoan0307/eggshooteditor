package ze.gamelogic.screen;

import ze.GMain;

import ze.gamegdx.Loader;
import ze.core.util.GLayer;
import ze.core.util.GScreen;
import ze.core.util.GStage;
import ze.gamelogic.ui.EditorUI;
import ze.gamelogic.ui.LoadingUI;
import ze.gamelogic.ui.TestUI;

public class MenuScreen extends GScreen {
    @Override
    public void dispose() {

    }

    @Override
    public void init() {
        new Loader();
        new EditorUI(GStage.getLayer(GLayer.ui));
    }

    @Override
    public void run() {

    }
    static boolean firstShow = false;
    public void show(){
        super.show();
        if(firstShow == false) {
            firstShow = true;
            GMain.me.getPlatform().OnShow();
        }
    }
}