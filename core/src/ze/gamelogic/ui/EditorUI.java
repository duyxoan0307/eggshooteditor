package ze.gamelogic.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.ArrayList;

import ze.core.util.GAssetsManager;
import ze.core.util.GStage;
import ze.gamegdx.ClickEvent;
import ze.gamegdx.Debug;
import ze.gamegdx.Loader;
import ze.gamegdx.Util;
import ze.gamegdx.gui.gui_extra.TextButton;
import ze.gamegdx.gui.tools.GUICreate;
import ze.gamegdx.gui.tools.GUITool;
import ze.gamegdx.gui.tools.UIGroup;
import ze.gamelogic.mvc.model.Level;
import ze.gamelogic.mvc.model.ListLevel;
import ze.gamelogic.mvc.view.EggView;
import ze.gamelogic.mvc.view.GamePlayView;

public class EditorUI extends UIGroup {
    public static boolean is_left_clicking=false;
    public static boolean is_right_clicking=false;
    public static int current_level=0;
    static TextureAtlas atlas;
    ScrollPane boardScroll,eggScroll;
    Table boardTable, eggTable;
    public static int egg_type_choosed=0;
    Json json = new Json();
    public static ListLevel listLevel;
    Label level_label;
    TextButton remove_row_bot_btn, create_rows_btn, remove_rows_btn, lock_scroll_btn;
    Table tableLeftMenu,tableRightMenu;
    TextField tfGoToLevel;
    public EditorUI(Group parent) {
        super(parent);
        Debug.Log("vkl");
        is_flick=true;
        setOdd(11);
        initRes();
        initUI();
        addEggsModel();
//        shuffleLevel();
    }

    public TextField tf, tfSpeed;

    private void createTextField() {
        TextField.TextFieldStyle tfs = new TextField.TextFieldStyle();
        tfs = new TextField.TextFieldStyle();
        tfs.font =Loader.font_white;
        tfs.fontColor = Color.BLACK;
        Texture tfSelection,tfBackground,tfCursor;
        tfSelection = new Texture(Gdx.files.internal("UI/tfSelection.png"));
        tfBackground = new Texture(Gdx.files.internal("UI/tfbackground.png"));
        tfCursor = new Texture(Gdx.files.internal("UI/cursor.png"));
        tfs.selection = new TextureRegionDrawable(new TextureRegion(tfSelection));
        tfs.background = new TextureRegionDrawable(new TextureRegion(tfBackground));
        tfs.cursor = new TextureRegionDrawable(new TextureRegion(tfCursor));
        tf = new TextField("", tfs);
        tf.setMessageText("rows...");
        tf.setMaxLength(20);
        tfs.background.setLeftWidth(20);
        tf.setTextFieldListener(new TextField.TextFieldListener() {
            public void keyTyped (TextField textField, char key) {
                if (key == '\n'){
                    textField.getOnscreenKeyboard().show(false);
                    Debug.Log("enter");
                }
            }
        });


        tf.setSize(200,100);
        tf.setPosition(remove_row_bot_btn.getX(),remove_row_bot_btn.getY()-20, Align.topLeft);

        tableLeftMenu.row();
        tableLeftMenu.add(tf);

        create_rows_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"CREATE_ROWS",Loader.font_white);
        tableLeftMenu.row();
        tableLeftMenu.add(create_rows_btn);
        create_rows_btn.title_label.setColor(Color.BLACK);
        create_rows_btn.setPosition(-GStage.getWidth()/2,tf.getY(Align.bottom)-20,Align.topLeft);
        create_rows_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                createRows(tf.getText());
            }
        });


        remove_rows_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"REMOVE_ROWS",Loader.font_white);
        tableLeftMenu.row();
        tableLeftMenu.add(remove_rows_btn);
        remove_rows_btn.title_label.setColor(Color.BLACK);
        remove_rows_btn.setPosition(-GStage.getWidth()/2,create_rows_btn.getY(Align.bottom)-20,Align.topLeft);
        remove_rows_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                removeRows(tf.getText());
            }
        });
    }
    TextField getTextField(){
        TextField tf;
        TextField.TextFieldStyle tfs = new TextField.TextFieldStyle();
        tfs = new TextField.TextFieldStyle();
        tfs.font =Loader.font_white;
        tfs.fontColor = Color.BLACK;
        Texture tfSelection,tfBackground,tfCursor;
        tfSelection = new Texture(Gdx.files.internal("UI/tfSelection.png"));
        tfBackground = new Texture(Gdx.files.internal("UI/tfbackground.png"));
        tfCursor = new Texture(Gdx.files.internal("UI/cursor.png"));
        tfs.selection = new TextureRegionDrawable(new TextureRegion(tfSelection));
        tfs.background = new TextureRegionDrawable(new TextureRegion(tfBackground));
        tfs.cursor = new TextureRegionDrawable(new TextureRegion(tfCursor));
        tf = new TextField("", tfs);
        tf.setMessageText("rows...");
        tf.setMaxLength(20);
        tfs.background.setLeftWidth(20);
        tf.setTextFieldListener(new TextField.TextFieldListener() {
            public void keyTyped (TextField textField, char key) {
                if (key == '\n'){
                    textField.getOnscreenKeyboard().show(false);
                    Debug.Log("enter");
                }
            }
        });


        tf.setSize(200,100);
        return tf;
    }
    private void createTextFieldTime() {
        TextField.TextFieldStyle tfs = new TextField.TextFieldStyle();
        tfs = new TextField.TextFieldStyle();
        tfs.font =Loader.font_white;
        tfs.fontColor = Color.BLACK;
        Texture tfSelection,tfBackground,tfCursor;
        tfSelection = new Texture(Gdx.files.internal("UI/tfSelection.png"));
        tfBackground = new Texture(Gdx.files.internal("UI/tfbackground.png"));
        tfCursor = new Texture(Gdx.files.internal("UI/cursor.png"));
        tfs.selection = new TextureRegionDrawable(new TextureRegion(tfSelection));
        tfs.background = new TextureRegionDrawable(new TextureRegion(tfBackground));
        tfs.cursor = new TextureRegionDrawable(new TextureRegion(tfCursor));
        tfSpeed = new TextField("", tfs);
        tfSpeed.setMessageText("time...");
        tfSpeed.setMaxLength(20);
        tfs.background.setLeftWidth(20);
        tfSpeed.setTextFieldListener(new TextField.TextFieldListener() {
            public void keyTyped (TextField textField, char key) {
                if (key == '\n'){
                    textField.getOnscreenKeyboard().show(false);
                    Debug.Log("enter");
                }
            }
        });


        tfSpeed.setSize(200,100);
        tfSpeed.getStyle().font.getData().setScale(0.6f);
        tfSpeed.setPosition(remove_row_bot_btn.getX(),remove_row_bot_btn.getY()-20, Align.topLeft);

        tableLeftMenu.row();
        tableLeftMenu.add(tfSpeed);
    }

    private void removeRows(String text) {
        try{
            int num = Integer.parseInt(text);
            for (int i=0;i<num;i++)
                removeRowBot();
        }
        catch (Exception e){

        }
    }

    private void createRows(String text) {
        try{
            int num = Integer.parseInt(text);
            for (int i=0;i<num;i++)
                newRowBottom();
        }
        catch (Exception e){

        }
    }


    private void initRes() {
        atlas=GAssetsManager.getTextureAtlas("eggAtlas");
    }

    private void initUI() {
        level_label = GUICreate.createLabel("",Loader.font_white);
        addActor(level_label);
        level_label.setAlignment(Align.topLeft);
        level_label.setPosition(-GStage.getWidth()/2+20,GStage.getHeight()/2-20,Align.topLeft);


        eggTable = new Table();
        eggScroll=new ScrollPane(eggTable);
        addActor(eggScroll);
        eggTable.defaults().pad(20).width(80).height(80);

        eggScroll.setSize(GStage.getWidth(),250);
        eggScroll.setPosition(0,-GStage.getHeight()/2+20,Align.bottom);

        boardTable=new Table();
        boardScroll=new ScrollPane(boardTable);
        addActor(boardScroll);
        boardTable.top();

        boardScroll.setSize(GStage.getWidth(),GStage.getHeight()-300);
        boardScroll.setPosition(0,GStage.getHeight()/2,Align.top);
//        boardTable.setDebug(true);
//        eggTable.setDebug(true);

        tableLeftMenu=new Table();
        tableLeftMenu.setSize(400,GStage.getHeight());
        addActor(tableLeftMenu);
        tableLeftMenu.setPosition(-GStage.getWidth()/2,-100,Align.left);
        tableLeftMenu.top().left();
        tableLeftMenu.defaults().pad(20);

        tableRightMenu=new Table();
        tableRightMenu.setSize(400,GStage.getHeight());
        addActor(tableRightMenu);
        tableRightMenu.setPosition(GStage.getWidth()/2,-100,Align.right);
        tableRightMenu.top().right();
        tableRightMenu.defaults().pad(20);

        TextButton new_row_top_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"TOP",Loader.font_white);
        tableLeftMenu.row();
        tableLeftMenu.add(new_row_top_btn);
        new_row_top_btn.title_label.setColor(Color.BLACK);
        new_row_top_btn.setPosition(-GStage.getWidth()/2,GStage.getHeight()/2-100,Align.topLeft);
        new_row_top_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                newRowTop();
            }
        });

        TextButton new_row_bot_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"BOT",Loader.font_white);
        tableLeftMenu.row();
        tableLeftMenu.add(new_row_bot_btn);
        new_row_bot_btn.title_label.setColor(Color.BLACK);
        new_row_bot_btn.setPosition(-GStage.getWidth()/2,new_row_top_btn.getY(Align.bottom)-20,Align.topLeft);
        new_row_bot_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                newRowBottom();
            }
        });
        TextButton remove_row_top_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"REMOVE_TOP",Loader.font_white);
        tableLeftMenu.row();
        tableLeftMenu.add(remove_row_top_btn);
        remove_row_top_btn.title_label.setColor(Color.BLACK);
        remove_row_top_btn.setPosition(-GStage.getWidth()/2,new_row_bot_btn.getY(Align.bottom)-20,Align.topLeft);
        remove_row_top_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                removeRowTop();
            }
        });
        remove_row_bot_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"REMOVE_BOT",Loader.font_white);
        tableLeftMenu.row();
        tableLeftMenu.add(remove_row_bot_btn);
        remove_row_bot_btn.title_label.setColor(Color.BLACK);
        remove_row_bot_btn.setPosition(-GStage.getWidth()/2,remove_row_top_btn.getY(Align.bottom)-20,Align.topLeft);
        remove_row_bot_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                removeRowBot();
            }
        });

        createTextField();

        lock_scroll_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"LOCK_SCROLL",Loader.font_white);
        tableLeftMenu.row();
        tableLeftMenu.add(lock_scroll_btn);
        lock_scroll_btn.title_label.setColor(Color.BLACK);
        lock_scroll_btn.setPosition(-GStage.getWidth()/2,remove_rows_btn.getY(Align.bottom)-20,Align.topLeft);
        lock_scroll_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onLockScroll();
            }
        });

        createTextFieldTime();

        TextButton test_speed_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"TEST_SPEED",Loader.font_white);
        tableLeftMenu.row();
        tableLeftMenu.add(test_speed_btn);
        test_speed_btn.title_label.setColor(Color.BLACK);
        test_speed_btn.setPosition(-GStage.getWidth()/2,remove_rows_btn.getY(Align.bottom)-20,Align.topLeft);
        test_speed_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onTestSpeed();
            }
        });

        TextButton save_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"SAVE",Loader.font_white);
//        addActor(save_btn);
        save_btn.title_label.setColor(Color.BLACK);
        save_btn.setPosition(-GStage.getWidth()/2,remove_row_bot_btn.getY(Align.bottom)-20,Align.topLeft);
        save_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                saveLevel();
            }
        });

        TextButton next_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"NEXT",Loader.font_white);
        tableRightMenu.row();
        tableRightMenu.add(next_btn);
        next_btn.title_label.setColor(Color.BLACK);
        next_btn.setPosition(GStage.getWidth()/2,new_row_top_btn.getY(Align.topRight),Align.topRight);
        next_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onNext();
            }
        });

        TextButton previous_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"PREVIOUS",Loader.font_white);
        tableRightMenu.row();
        tableRightMenu.add(previous_btn);
        previous_btn.title_label.setColor(Color.BLACK);
        previous_btn.setPosition(next_btn.getX(),next_btn.getY(Align.bottom)-20,Align.topLeft);
        previous_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onPrevious();
            }
        });
        TextButton recover_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"RESTORE",Loader.font_white);
        tableRightMenu.row();
        tableRightMenu.add(recover_btn);
        recover_btn.title_label.setColor(Color.BLACK);
        recover_btn.setPosition(next_btn.getX(),previous_btn.getY(Align.bottom)-20,Align.topLeft);
        recover_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onRecover();
            }
        });
        TextButton remove_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"REMOVE",Loader.font_white);
        tableRightMenu.row();
        tableRightMenu.add(remove_btn);
        remove_btn.title_label.setColor(Color.BLACK);
        remove_btn.setPosition(next_btn.getX(),recover_btn.getY(Align.bottom)-20,Align.topLeft);
        remove_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onRemove();
            }
        });

        TextButton new_level_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"NEW_LEVEL",Loader.font_white);
        tableRightMenu.row();
        tableRightMenu.add(new_level_btn);
        new_level_btn.title_label.setColor(Color.BLACK);
        new_level_btn.setPosition(next_btn.getX(),remove_btn.getY(Align.bottom)-20,Align.topLeft);
        new_level_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onNewLevel();
            }
        });

        tfGoToLevel=getTextField();
        tfGoToLevel.setMessageText("level...");
        tableRightMenu.row();
        tableRightMenu.add(tfGoToLevel);

        TextButton go_to_level_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"GO_TO_LEVEL",Loader.font_white);
        tableRightMenu.row();
        tableRightMenu.add(go_to_level_btn);
        go_to_level_btn.title_label.setColor(Color.BLACK);
        go_to_level_btn.setPosition(next_btn.getX(),remove_btn.getY(Align.bottom)-20,Align.topLeft);
        go_to_level_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onGoToLevel();
            }
        });

        type_level_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"as",Loader.font_white);
        type_level=0;
        tableRightMenu.row();
        tableRightMenu.add(type_level_btn);
        type_level_btn.title_label.setColor(Color.BLACK);
        type_level_btn.title_label.setFontScale(0.7f);
        type_level_btn.setPosition(0,00,Align.bottomRight);
        type_level_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onChangeTypeLevel();
            }
        });
        onChangeTypeLevel();


        TextButton hide_all_egg_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,80),"HIDE ALL",Loader.font_white);
        tableRightMenu.row();
        tableRightMenu.add(hide_all_egg_btn);
        hide_all_egg_btn.title_label.setColor(Color.BLACK);
        hide_all_egg_btn.title_label.setFontScale(0.7f);
        hide_all_egg_btn.setPosition(0,00,Align.bottomRight);
        hide_all_egg_btn.addListener(new ClickEvent(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                onHideAllEgg();
            }
        });
    }
    void onHideAllEgg(){
        for (int i=0;i<boardTable.getChildren().size;i++){
            Table table=(Table)boardTable.getChildren().get(i);
            int row[] =new int[table.getChildren().size];
            for (int j=0;j<table.getChildren().size;j++){
                EggView eggView=(EggView)table.getChildren().get(j);
                eggView.setVisible(false);
            }
        }
    }
    private void onChangeTypeLevel() {
        type_level++;
        type_level=type_level%3;
        setTypeLevelBtn();
        loadListLevel();
        onNewLevel();
    }

    private void setTypeLevelBtn() {
        String file_name="";
        if (type_level==0){;
            file_name="map";
        }else if(type_level==1){
            file_name="adventure";
        }else{
            file_name="time";
        }
        file_location="level_data/"+file_name+".txt";
        type_level_btn.title_label.setText(file_name);
    }

    TextButton type_level_btn;
    // NONSPEED-SPEED
    int type_level;
    private void onGoToLevel() {
        try {
            current_level=Integer.parseInt(tfGoToLevel.getText());
            checkCurrentLevel();
            loadData();
        }catch (Exception e){

        }
    }

    private void onTestSpeed() {
        this.is_move_scroll=!this.is_move_scroll;
        if (!this.is_move_scroll){
            boardScroll.setPosition(0,GStage.getHeight()/2,Align.top);
            boardScroll.setActor(boardTable);
        }else{
            boardScroll.scrollTo(0,0,0,0);
            GUITool.addActorNotChangePosition(boardTable,this);
            boardTable.setPosition(0,boardScroll.getY(Align.bottom),Align.bottom);
        }
    }

    boolean is_flick;
    private void onLockScroll() {
        is_flick=!is_flick;
        boardScroll.setFlickScroll(is_flick);
        String s="LOCK";
        if (!is_flick)
            s="UNLOCK";
        lock_scroll_btn.title_label.setText(s);
    }

    private void onNewLevel() {
        current_level=listLevel.levels.size;
        loadData();
    }

    private void onRecover() {
        loadData();
    }

    private void onRemove() {
        if (current_level>=listLevel.levels.size)
        {
            onPrevious();
            return;
        }
        if (listLevel.levels.size<=1)
            return;
        listLevel.levels.removeIndex(current_level);
        saveData();
        if (current_level>=listLevel.levels.size)
            current_level=listLevel.levels.size-1;
        checkCurrentLevel();
        loadData();
    }

    private void onPrevious() {
        current_level--;
        checkCurrentLevel();
        loadData();
    }

    private void loadData() {
        boardTable.clearChildren();
        tfSpeed.setText("0");
        if (current_level>=listLevel.levels.size){
            newFirstRow(ODD);
        }else{
            Level level=listLevel.levels.get(current_level);
            for (int i=0;i<level.rows.size;i++){
                int[] row = level.rows.get(i);
                newRowBottom(row.length);
                Table table = (Table)boardTable.getChildren().peek();
                for (int j=0;j<row.length;j++){
                    EggView eggView=(EggView)table.getChildren().get(j);
                    eggView.setEgg(row[j]);
                }
            }
            tfSpeed.setText(level.speed+"");
            if (type_level==2)
                tfSpeed.setText(level.time+"");
        }
        setLevel();
    }

    private void checkCurrentLevel() {
        if (current_level<0)
            current_level=0;
        else if (current_level>listLevel.levels.size)
            current_level=listLevel.levels.size;
    }

    private void onNext() {
        saveLevel();
        current_level++;
        checkCurrentLevel();
        loadData();
    }

    static String file_location="level_data/adventure.txt";
    void setFile(String path){
        file_location=path;
    }
    void loadListLevel(){
        FileHandle fileHandle=Gdx.files.local(file_location);
        try{
            String s_level =fileHandle.readString();
            if (s_level.equals(""))
                return;
            JsonReader jsonReader=new JsonReader();
            JsonValue jsonValue= jsonReader.parse(s_level);
            json.setElementType(ListLevel.class,"levels",Level.class);


            listLevel=new ListLevel();

            JsonValue _array = jsonValue.get("levels");
            for (int i=0;i<_array.size;i++){
                JsonValue array=_array.get(i);
                Array<int[]> arr =new Array<int[]>();
                int index = array.getInt("index");
                JsonValue rows = array.get("rows");
                for (int j=0;j<rows.size;j++){
                    int count = rows.get(j).size;
                    int[] array_rows = new int[count];
                    for (int k=0;k<count;k++){
                        array_rows[k]=rows.get(j).getInt(k);
                    }
                    arr.add(array_rows);
                }
                float speed=0;
                try{
                    speed=array.getFloat("speed");
                }catch (Exception e){}
                int time=0;
                try{
                    time=array.getInt("speed");
                }catch (Exception e){}
                Level level=new Level(index,arr,speed,time);
                listLevel.levels.add(level);
            }
        }
        catch (Exception e){

        }
    }

    void saveLevel() {
        int index=current_level;
        float speed =0;
        try{
            speed=Float.parseFloat(tfSpeed.getText());
        }catch (Exception e){}
        float time =0;
        try{
            time=Float.parseFloat(tfSpeed.getText());
        }catch (Exception e){}
        Level level=new Level(index,new Array<int[]>(),speed,(int)time);
        for (int i=0;i<boardTable.getChildren().size;i++){
            Table table=(Table)boardTable.getChildren().get(i);
            int row[] =new int[table.getChildren().size];
            for (int j=0;j<table.getChildren().size;j++){
                EggView eggView=(EggView)table.getChildren().get(j);
                row[j]=eggView.getType();
            }
            level.rows.add(row);
        }
        if (index>=listLevel.levels.size){
            listLevel.levels.add(level);
        }
        else{
            listLevel.levels.set(current_level,level);
        }
        saveData();
    }
    void saveData(){
        String json_level = json.toJson(listLevel);
        Gdx.files.local(file_location).writeString(json.prettyPrint(json_level),false);
    }
    void newFirstRow(int count){
        newRowTop(count);
    }
    void newRowTop(){
        Actor actor = boardTable.getChildren().first();
        int count;
        Table table=(Table)actor;
        if (!isOdd(table.getChildren().size))
            count=ODD;
        else
            count=EVEN;
        newRowTop(count);
    }
    void newRowBottom(){
        Actor actor = boardTable.getChildren().peek();
        int count;
        Table table=(Table)actor;
        if (!isOdd(table.getChildren().size))
            count=ODD;
        else
            count=EVEN;
        newRowBottom(count);
        boardScroll.scrollTo(0,0,0,0);
    }
    void newRowTop(int count){
        Table table = newEggTable(count);
        Array<Actor> array=new Array<Actor>(boardTable.getChildren());
        boardTable.clearChildren();

        boardTable.add(table);
        for (Actor actor:array){
            boardTable.row();
            boardTable.add(actor);
        }
        boardScroll.scrollTo(0,boardTable.getHeight(),0,0);
    }

    Table newEggTable(int count){
        Table table = new Table();
        table.center();
        table.defaults().width(70).height(70);
        for (int i=0;i<count;i++)
        {
            Image egg = newEgg();
//            egg.setVisible(false);
            table.add(egg);
        }
        return table;
    }

    EggView newEgg(){
        final EggView egg = new EggView(atlas.findRegion("e1"),egg_type_choosed);
        return egg;
    }

    void newRowBottom(int count){
        Table table = newEggTable(count);
        boardTable.row();
        boardTable.add(table);
    }

    static final int count_egg =13;
    private void addEggsModel() {
        for (int i=0;i<count_egg;i++){
            Image egg = new Image(atlas.findRegion("e0"));
            setEggType(egg,i);
//            if (i==5 || i==7)
//                egg.setVisible(false);
            eggTable.add(egg);
            if ((i+1)%7==0)
                eggTable.row();
            final int finalI = i;
            egg.addListener(new ClickEvent(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    egg_type_choosed= finalI;
                    updateEggTypeChoosed();
                }
            });
        }
        updateEggTypeChoosed();
    }

    private void updateEggTypeChoosed() {
        for (Actor actor:eggTable.getChildren())
            actor.getColor().a=0.7f;
        eggTable.getChildren().get(egg_type_choosed).getColor().a=1;
    }

    public static void setEggType(Image egg_img, int index){
        if (index<0)
            index=0;
        String[] textures = {"Bubble_Blue","Bubble_Cyan","Bubble_Green","Bubble_Pink","Bubble_Red","Bubble_Yel","Bubble_White","Bubble_Purple","Bubble_Oran","Bubble_Grey","Bubble_Frozen","Bubble_BlackHole","Bubble_Question"};
        egg_img.setDrawable(new TextureRegionDrawable(Util.findRegion(atlas,"e"+index)));
    }
    void removeRowTop(){
        Array<Actor> array=new Array<Actor>(boardTable.getChildren());
        if (array.size==1)
            return;
        array.removeIndex(0);
        boardTable.clearChildren();
        for (Actor actor:array){
            boardTable.row();
            boardTable.add(actor);
        }
    }
    void removeRowBot(){
        Array<Actor> array=new Array<Actor>(boardTable.getChildren());
        if (array.size==1)
            return;
        array.removeIndex(array.size-1);
        boardTable.clearChildren();
        for (Actor actor:array){
            boardTable.row();
            boardTable.add(actor);
        }
    }
    void setLevel(){
        level_label.setText("LEVEL "+current_level+"/"+(listLevel.levels.size-1));
    }
    @Override
    public void act(float var1) {
        super.act(var1);
        moveScroll();
    }

    boolean is_move_scroll=false;
    float speed=0;
    private void moveScroll() {
        if (!is_move_scroll)
            return;
        boardTable.moveBy(0,-getSpeed());
    }
    float getSpeed(){
        float speed =0 ;
        try{
            speed=Float.parseFloat(tfSpeed.getText());
        }
        catch (Exception e){}
        return speed;
    }

    public static boolean isOdd(int count){
        return count==ODD;
    }
    public static int ODD,EVEN;
    void setOdd(int odd){
        ODD=odd;
        EVEN=ODD-1;
    }
    public void shuffleLevel(){
        file_location="level_data/adventure.txt";
        loadListLevel();
        ListLevel list_1 = new ListLevel();
        ListLevel list_2 = new ListLevel();
        list_1.levels.addAll(listLevel.levels);

        file_location="level_data/map.txt";
        loadListLevel();
        list_2.levels.addAll(listLevel.levels);
        ListLevel gather =  gatherLevel(list_1,list_2);


        listLevel.levels=new Array<Level>(gather.levels);
        for (int i=0;i<listLevel.levels.size;i++){
            listLevel.levels.get(i).index=i;
        }
        saveData();
    }

    private ListLevel gatherLevel(ListLevel list_1, ListLevel list_2) {
        ListLevel listLevel=new ListLevel();
        while (list_1.levels.size>0 && list_2.levels.size>0){
            Level level_1 = list_1.levels.first();
            list_1.levels.removeValue(level_1,false);
            Level level_2 = list_2.levels.first();
            list_2.levels.removeValue(level_2,false);
            listLevel.levels.add(level_1);
            listLevel.levels.add(level_2);
        }
        listLevel.levels.addAll(list_1.levels);
        listLevel.levels.addAll(list_2.levels);
        return listLevel;
    }
}
