package ze.gamelogic.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

import ze.gamegdx.Loader;
import ze.gamegdx.gui.tools.TransitionUtil;
import ze.gamegdx.Util;
import ze.gamegdx.gui.tools.GUICreate;
import ze.gamegdx.gui.tools.UIGroup;
import ze.core.util.GLayer;
import ze.core.util.GStage;

public class LoadingUI extends UIGroup {
    static LoadingUI instance;
    float time=0;
    float duration=0.3f;
    int count_loading=0;
    Label loading_label;

    public LoadingUI(Group parent) {
        super(parent);
        initRes();
        initUI();
    }

    @Override
    protected void setAnimationShow(SequenceAction sequence_action) {
        super.setAnimationShow(TransitionUtil.animationShowFadeIn());
    }

    @Override
    protected void setAnimationHide(SequenceAction sequence_action) {
        super.setAnimationHide(TransitionUtil.animationHideFadeOut());
    }

    private void initRes() {
    }
    private void initUI() {
        loading_label = GUICreate.createLabel("LOADING...", Loader.font_white);
        groupUI.addActor(loading_label);
        loading_label.setPosition(0,0,Align.center);
        loading_label.setAlignment(Align.left);
    }
    @Override
    public void act(float var1) {
        super.act(var1);
        time+= Gdx.graphics.getDeltaTime();
        if (time>duration){
            time=0;
            count_loading++;
            count_loading=count_loading%4;
            String s_dot="";
            for (int i=0;i<count_loading;i++)
                s_dot+=".";
            loading_label.setText("LOADING"+s_dot);
        }
    }
    public static void showLoading(){
        if (instance!=null)
            instance.remove();
        instance=new LoadingUI(GStage.getLayer(GLayer.ui));
        Util.delayFunction(new Util.OndelayCallback() {
            @Override
            public void OnEvent() {
                hideLoading();
            }
        },2f,instance);
    }
    public static void hideLoading(){
        if (instance!=null)
            instance.hide();
    }
}
