package ze.gamelogic.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import ze.GMain;
import ze.core.util.GAssetsManager;
import ze.gamegdx.Debug;
import ze.gamegdx.Loader;
import ze.gamegdx.Util;
import ze.gamegdx.gui.gui_extra.TextButton;
import ze.gamegdx.gui.tools.GUICreate;
import ze.gamegdx.gui.tools.UIGroup;
import ze.platform.IPlat;

public class TestUI extends UIGroup {
    public static TestUI instance;
    public TestUI(Group parent) {
        super(parent);
        instance=this;
        initRes();
        initUI();
        test();
    }

    private void test() {
        SendRequest("http://static.bonanhem.com/daovang/level_v1.xml", new IPlat.HTTP_RequestCallback() {
            @Override
            public void OnValue(String s) {
                Debug.Log(s);
            }
        });
    }
    public static void SendRequest(final String url, final IPlat.HTTP_RequestCallback callback){
        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        final Net.HttpRequest httpRequest = requestBuilder.newRequest().method(Net.HttpMethods.GET).url(url).build();
        httpRequest.setHeader(HttpRequestHeader.UserAgent,"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0");
        Gdx.net.sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
//                Utilities.consolelog(url+" success");
                if (callback!=null)
                    callback.OnValue(httpResponse.getResultAsString());
            }

            @Override
            public void failed(Throwable t) {
            }

            @Override
            public void cancelled() {

            }
        });
    }

    private void initRes() {
    }
    private void initUI() {
        final TextButton share_btn = GUICreate.createTextButton(GUICreate.createTextureRegion(200,100),
                "Share",Loader.font_white);
        addActor(share_btn);
        share_btn.title_label.setColor(Color.BLACK);
        share_btn.setPosition(0,0,Align.center);
        share_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                GMain.getPlatform().FBInstant_ShareAsync(Util.getScreenshot(), new IPlat.FBInstant_ContextCallback() {
                    @Override
                    public void OnSuccess() {
                        Debug.Log("ShareAsync","successfully");
                    }

                    @Override
                    public void OnFailed(String data) {
                        Debug.Log("ShareAsync","error");
                        Debug.Log(data);
                    }
                });
            }
        });


        Button invite_btn = GUICreate.creatButton(GUICreate.createTextureRegion(200,100));
        addActor(invite_btn);
        invite_btn.setPosition(0,-200,Align.center);
        invite_btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                Debug.Log("get screen shot!");
                Pixmap pixmap=Util.getScreenshot();
                Image img = new Image(new Texture(pixmap));
//                img.setDebug(true);
                addActor(img);
                img.setSize(200,200);
                img.setPosition(-300,-300);
                img.getColor().a=1;
                img.setColor(Color.YELLOW);

                String base64URL = GMain.getPlatform().getBase64URL(pixmap);
                Debug.Log("Base64URL",base64URL);
            }
        });
    }
}
