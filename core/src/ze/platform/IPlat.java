package ze.platform;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

public interface IPlat {
    public void log(String str);

    public void LoadUrlTexture(String url, final UrlTextureCallback callback);

    public static interface UrlTextureCallback {
        public void success(Texture texture);
        public void error();
    }


    public Pixmap getFrameBufferPixmap(int x, int y, int w, int h);
    public String getBase64URL(Pixmap pixmap);
    public String GetNumberFormat(long number);
    public String GetNumberDotFormat(long number);
    public void FBInstant_GetJsonStats(FBInstant_GetJsonStatsCallback callback);
    public void FBInstant_SetJsonStats(String value, FBInstant_SetJsonStatsCallback callback);
    public void FBInstant_GetStringData(String statname, FBInstant_GetStringStatsCallback callback);
    public void FBInstant_SetStringData(String statname, String value, FBInstant_SetStringStatsCallback callback);
    public void FBInstant_GetDoubleStats(String statname, FBInstant_GetStatsCallback callback);
    public void FBInstant_SetDoubleStats(String statname, double value, FBInstant_SetStatsCallback callback);
    public void FBInstant_IncrementDoubleStats(String statname, double value, FBInstant_IncrementDoubleStatsCallback callback);
    public void FBInstant_LoadLeaderboard(FBInstant_LeaderboardEntryCallback callback);
    public void FBInstant_LoadLeaderboardFriend(FBInstant_LeaderboardEntryCallback callback);
    public void FBInstant_GetPlayerInfo(FBInstant_PlayerInfoCallback callback);
    public String FBInstant_GetPlayerName();
    public String FBInstant_GetPlayerPhoto();
    public String FBInstant_GetPlayerID();
    public boolean FBInstant_IsPreloadLoadingFinished(int num);
    public void FBInstant_StartLoadingPreload(int num);
    public void FBInstant_ReportScore(double score);
    public void OnShow();
    public void FBInstant_CanCreateShortCut(FBInstant_CanCreateCallback callback);
    public void FBInstant_CreateShortCut();
    public void FBInstant_CanSubscribeBot();
    public String FBInstant_GetDataContext();
    public void FBInstant_ShareAsync(Pixmap pixmap, FBInstant_ContextCallback callback);
    public void FBInstant_InviteAsync(FBInstant_ContextCallback callback);
    public void FBInstant_UpdateAsync(String data, FBInstant_ContextCallback callback);
    public String FBInstant_Getlocale();

    public static interface FBInstant_StartCallback{
        public void OnEvent();
    }

    public static interface FBInstant_CanCreateCallback{
        public void OnCallback(boolean canCreateShortcut);
    }
    public static interface FBInstant_GetJsonStatsCallback {
        public void OnValue(String jsonValue, boolean undefined);
    }

    public static interface FBInstant_SetJsonStatsCallback {
        public void OnSuccess(boolean success);
    }

    public static interface FBInstant_GetStringStatsCallback {
        public void OnValue(String value, boolean undefined);
    }

    public static interface FBInstant_SetStringStatsCallback {
        public void OnSuccess(boolean success);
    }


    public static interface FBInstant_GetStatsCallback {
        public void OnValue(double value, boolean undefined);
    }

    public static interface FBInstant_SetStatsCallback {
        public void OnSuccess(boolean success);
    }

    public static interface FBInstant_IncrementDoubleStatsCallback {
        public void OnSuccess(double newvalue);
    }

    public static interface FBInstant_LeaderboardEntryCallback {
        public void OnEntry(String data);
    }

    public static interface FBInstant_PlayerInfoCallback {
        public void OnInfo(String name, String photoUrl);
    }
    public static interface FBInstant_ContextCallback {
        public void OnSuccess();
        public void OnFailed(String data);
    }

    public void ReportScore(String leaderboardID, long score);

    public void ShowFullscreen();
    public void ShowBanner(boolean visible);

    public void TrackLevelStart(int level);
    public void TrackLevelFailed(int level);
    public void TrackLevelCompleted(int level);
    public void ShowLeaderBoard();
    public boolean IsVideoRewardReady();
    public void ShowVideoReward(OnVideoRewardClosed callback);

    public interface OnVideoRewardClosed{
        public void OnEvent(boolean success);
    }
    public interface HTTP_RequestCallback{
        public void OnValue(String s);
    }
    //callback
}
