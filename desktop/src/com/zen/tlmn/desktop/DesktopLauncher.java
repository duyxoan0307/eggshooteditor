package com.zen.tlmn.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.ScreenUtils;
import ze.platform.IPlat;
import ze.GMain;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;

public class DesktopLauncher {

	public static GMain game;
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 848;
		config.height = 848;


		IPlat plat = new IPlat() {
			@Override
			public void log(String str) {

			}

			@Override
			public void LoadUrlTexture(String url, final UrlTextureCallback callback) {
				new Thread(new Runnable() {

					@Override
					public void run () {
						try {
							String url2 = "http://img.hb.aicdn.com/a61481e41098c6f1ca1ec5850085476f86cb89283604d-x6leM5_/fw/480";
							byte[] bytes = SendGetRequestToByte(url2);
							if (bytes != null) {
								// load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
								final Pixmap pixmap = new Pixmap(bytes, 0, bytes.length);
								final int originalWidth = pixmap.getWidth();
								final int originalHeight = pixmap.getHeight();
								int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
								int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
								final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
								potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
								pixmap.dispose();
								Gdx.app.postRunnable(new Runnable() {
									@Override
									public void run() {
										Texture txt = new Texture(potPixmap);
										callback.success(txt);
									}
								});
							}
						}
						catch (Exception e){

						}
					}
				}).start();
			}

			@Override
			public void FBInstant_GetJsonStats(FBInstant_GetJsonStatsCallback callback) {

			}

			@Override
			public void FBInstant_SetJsonStats(String value, FBInstant_SetJsonStatsCallback callback) {

			}

			@Override
			public void FBInstant_GetStringData(String statname, FBInstant_GetStringStatsCallback callback) {

			}

			@Override
			public void FBInstant_SetStringData(String statname, String value, FBInstant_SetStringStatsCallback callback) {

			}

			@Override
			public void FBInstant_GetDoubleStats(String statname, FBInstant_GetStatsCallback callback) {

			}

			@Override
			public void FBInstant_SetDoubleStats(String statname, double value, FBInstant_SetStatsCallback callback) {

			}

			@Override
			public void FBInstant_IncrementDoubleStats(String statname, double value, FBInstant_IncrementDoubleStatsCallback callback) {

			}

			@Override
			public void FBInstant_LoadLeaderboard(FBInstant_LeaderboardEntryCallback callback) {
				if (callback!=null)
					callback.OnEntry("");
			}

			@Override
			public void FBInstant_LoadLeaderboardFriend(FBInstant_LeaderboardEntryCallback callback) {
				if (callback!=null)
					callback.OnEntry("");
			}

			@Override
			public void FBInstant_GetPlayerInfo(FBInstant_PlayerInfoCallback callback) {

			}

			@Override
			public String FBInstant_GetPlayerName() {
				return null;
			}

			@Override
			public String FBInstant_GetPlayerPhoto() {
				return null;
			}

			@Override
			public String FBInstant_GetPlayerID() {
				return null;
			}

			@Override
			public boolean FBInstant_IsPreloadLoadingFinished(int num) {
				return false;
			}

			@Override
			public void FBInstant_StartLoadingPreload(int num) {

			}

			@Override
			public void FBInstant_ReportScore(double score) {

			}

			@Override
			public void OnShow() {

			}

			@Override
			public void FBInstant_CanCreateShortCut(FBInstant_CanCreateCallback callback) {

			}

			@Override
			public void FBInstant_CreateShortCut() {

			}

			@Override
			public void FBInstant_CanSubscribeBot() {

			}

			@Override
			public String FBInstant_GetDataContext() {
				return null;
			}

			@Override
			public void FBInstant_ShareAsync(Pixmap pixmap, FBInstant_ContextCallback callback) {

			}

			@Override
			public void FBInstant_InviteAsync(FBInstant_ContextCallback callback) {

			}

			@Override
			public void FBInstant_UpdateAsync(String data, FBInstant_ContextCallback callback) {

			}

			@Override
			public String FBInstant_Getlocale() {
				return null;
			}

			@Override
			public void ReportScore(String leaderboardID, long score) {

			}

			@Override
			public void ShowFullscreen() {

			}

			@Override
			public void ShowBanner(boolean visible) {

			}

			@Override
			public void TrackLevelStart(int level) {

			}

			@Override
			public void TrackLevelFailed(int level) {

			}

			@Override
			public void TrackLevelCompleted(int level) {

			}

			@Override
			public void ShowLeaderBoard() {

			}

			@Override
			public boolean IsVideoRewardReady() {
				return true;
			}

			@Override
			public void ShowVideoReward(OnVideoRewardClosed callback) {

			}

			@Override
			public Pixmap getFrameBufferPixmap(int x, int y, int w, int h) {

				byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), true);

				for(int i = 4; i < pixels.length; i += 4) {
					pixels[i - 1] = (byte) 255;
				}

				Pixmap pixmap = new Pixmap(Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), Pixmap.Format.RGBA8888);
				BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
				return pixmap;
			}

			@Override
			public String getBase64URL(Pixmap pixmap) {
				return "dataURL";
			}

			@Override
			public String GetNumberFormat(long number) {
				NumberFormat df2=NumberFormat.getInstance();
				df2.setMaximumFractionDigits(2);
				df2.setMinimumFractionDigits(0);
				if (number<1000)
					return df2.format(((float)number))+"";
				if (number<1000000)
					return df2.format(((float)number/1000)) +"K";
				if (number<1000000000)
					return df2.format(((float)number/1000000))+"M";
				return df2.format(((float)number/1000000000))+"B";
			}

            @Override
            public String GetNumberDotFormat(long number) {
                return number+"";
            }
        };

		game = new GMain(plat);
		new LwjglApplication(game, config);

	}

	public static byte[] SendGetRequestToByte(String urlStr) {
		try {
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.addRequestProperty("User-Agent", "Mozilla/5.0");
			conn.connect();
			// Get the response
			InputStream in = conn.getInputStream();
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead=in.read(data,0,data.length))!=-1) buffer.write(data,0,nRead);
			buffer.flush();
			return buffer.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
