package com.zen.tlmn;

import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.uikit.UIApplication;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;
import com.zen.tlmn.TLMN;

public class IOSLauncher extends IOSApplication.Delegate {
    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();

        IZen zenObj = new IZen() {
            @Override
            public void ShowFullscreen() {
                Gdx.app.log("SDK", "Full Screen");
            }

            @Override
            public void ShowBanner(boolean visible) {
                Gdx.app.log("SDK", "Banner");
            }

            @Override
            public void TrackLevelStart(int level) {
                Gdx.app.log("SDK", "Level Start " + level);
            }

            @Override
            public void TrackLevelFailed(int level) {
                Gdx.app.log("SDK", "Level Failed " + level);
            }

            @Override
            public void TrackLevelCompleted(int level) {
                Gdx.app.log("SDK", "Level Complete " + level);
            }

            @Override
            public void ShowLeaderBoard() {
                Gdx.app.log("SDK", "Show LeaderBoard");
            }

            @Override
            public void ReportScore(String leaderboardID, int score) {
                Gdx.app.log("SDK", "Report Score");
            }

            @Override
            public void Rate() {
                Gdx.app.log("SDK", "Rate");
            }

            @Override
            public void Like() {
                Gdx.app.log("SDK", "Like");
            }
        };

        return new IOSApplication(new TLMN(zenObj), config);
    }

    public static void main(String[] argv) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, IOSLauncher.class);
        pool.close();
    }
}